CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Support
 * Maintainers

INTRODUCTION
------------

The content snapshot module provides a mechanism to export all content to a selected "snapshot directory", and import
it later.

It doesn't allow for syncing individual content pieces: on export it clears the snapshot directory and replaces its
content with a new snapshot. On import it deletes all content from the database, and then imports content snapshot
from the snapshot directory.

The module is being fully operated by using CLI and is intended for automated workflows/working on the same content
by multiple developers, without database sharing. It allows you to create Drupal instance filled with hand crafted
content from the scratch, provided you can set up other steps that are required for that.


INSTALLATION
------------

Install it by using composer. It's for automated workflows, you really shouldn't install it manually.


CONFIGURATION
-------------

The default snapshot path is "../content/snapshot" (it's relative to Drupal's index.php). You need to create this
directory, and create an empty ".snapshot" file there.

Default settings of this module can be found in the "./config/install/content_snapshot.settings.yml" file. Config
schema is described in "./config/schema/content_snapshot.schema.yml". You can override these values by using Drupal's
configuration override system.
See: https://www.drupal.org/docs/drupal-apis/configuration-api/configuration-override-system

"strict" setting means "strict level" the module operates in. Strict level tells module how to behave during the
import, in the situation where there is data in snapshot that cannot be imported (because, for example, field
configuration has been removed from the database after the last snapshot was made).

There are three strict levels available: 0, 1 and 2

0: During the import, silently ignore data that cannot be imported.
1: During the import, display a warning for each case of data that cannot be imported.
2: Interrupt import progress with an exception if data that cannot be imported is encountered.

USAGE
-------------
This module provides you with new drush commands:

- content-snapshot:export
- content-snapshot:import

It doesn't show any progress by default, but you can increase verbocity by adding "-v" parameter.


There is an "--snapshot-path" option available, allowing for overriding snapshot path.

E.g.:
drush content-snapshot:export --snapshot-path=mynewpath


SUPPORT
-------

This open source project is supported by the Drupal.org community. To report a bug, request a feature, or upgrade to
the latest version, please visit the project page: https://drupal.org/project/content_snapshot


MAINTAINERS
-----------

Łukasz Zaroda
https://www.drupal.org/u/luke_nuke
