<?php

/**
 * @file
 */

namespace Drupal\Tests\content_snapshot\Functional\modules\paragraphs;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\Tests\content_snapshot\Functional\AbstractFunctional;

/**
 * Class ParagraphsTest.
 *
 * This class tests paragraphs module handling.
 *
 * @coversDefaultClass \Drupal\content_snapshot\Commands\Commands
 *
 * @group content_snapshot
 */
class ParagraphsTest extends AbstractFunctional {

  private CONST TEST_PARAGRAPH_TYPE = 'test_paragraph';

  protected static $modules = [
    'node',
    'content_snapshot',
    'paragraphs',
  ];

  /**
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpSnapshotPath();

    $this->nodeStorage = $this->entityTypeManager->getStorage('node');

    $this->createContentType([
      'type' => 'page',
      'name' => 'Page type',
    ]);

    ParagraphsType::create([
      'id' => self::TEST_PARAGRAPH_TYPE,
      'label' => 'Test paragraph'
    ])->save();

    FieldStorageConfig::create([
      'type' => 'entity_reference_revisions',
      'field_name' => 'field_paragraphs',
      'entity_type' => 'node',
      'settings' => [
        'target_type' => 'paragraph',
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_paragraphs',
      'field_type' => 'entity_reference_revisions',
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => 'Paragraphs',
    ])->save();

    // Create field_body for paragraph's body.

    FieldStorageConfig::create(array(
    'field_name' => 'field_body',
    'entity_type' => 'paragraph',
    'type' => 'text_long',
    'cardinality' => -1,
    ))->save();

    FieldConfig::create([
      'field_name' => 'field_body',
      'field_type' => 'text_long',
      'entity_type' => 'paragraph',
      'bundle' => 'test_paragraph',
      'label' => 'Paragraph Body',
    ])->save();

  }

  /**
   * Tests if paragraphs export import export produces consistent snapshots.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testExportImportExportCycle() {
    $this->createNode([
      'type' => 'page',
      'field_paragraphs' => Paragraph::create([
        'type' => self::TEST_PARAGRAPH_TYPE,
        'field_body' => 'Lorem ipsum',
      ]),
    ])->save();

    $this->assertSnapshotConsistency();
  }

  /**
   * Tests if node has paragraph reference after snapshot restoration.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function testNodeParagraphReference() {
    $this->createNode([
      'type' => 'page',
      'field_paragraphs' => Paragraph::create([
        'type' => self::TEST_PARAGRAPH_TYPE,
      ]),
    ])->save();

    $this->runSnapshotExportImport();

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load(1);

    $value = $node->get('field_paragraphs')->first()->getValue();

    self::assertEquals(1, (int) $value['target_id']);
  }

  /**
   * Tests if paragraph's text field has correct value after snapshot
   * restoration.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function testBody() {
    $bodyValue = 'Lorem ipsum';

    $this->createNode([
      'type' => 'page',
      'field_paragraphs' => Paragraph::create([
        'type' => self::TEST_PARAGRAPH_TYPE,
        'field_body' => $bodyValue,
      ]),
    ])->save();

    $this->runSnapshotExportImport();

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load(1);

    $value = $node->get('field_paragraphs')->first()->getValue();

    $paragraphStorage = $this->entityTypeManager->getStorage('paragraph');

    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    $paragraph = $paragraphStorage->load($value['target_id']);

    $body = $paragraph->get('field_body')->first()->getValue();
    self::assertEquals($bodyValue, $body['value']);
  }

}
