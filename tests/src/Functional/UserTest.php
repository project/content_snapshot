<?php

namespace Drupal\Tests\content_snapshot\Functional;

/**
 * Class UserTest.
 *
 * This class tests users snapshots.
 *
 * @group content_snapshot
 */
class UserTest extends AbstractFunctional {

  protected static $modules = [
    'node',
    'content_snapshot',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpSnapshotPath();
  }

  /**
   * Tests additional users import/export cycle .
   */
  public function testExportImportExportCycle() {
    $this->drupalCreateUser();
    $this->assertSnapshotConsistency();
  }

  /**
   * Tests if created user can still log in after export/import cycle.
   */
  public function testLogin() {
    $pass =  $this->randomMachineName();

    $this->drupalCreateUser(['administer modules'], FALSE, FALSE, ['pass' => $pass]);

    $this->runSnapshotExportImport();

    /** @var \Drupal\user\UserStorageInterface $userStorage */
    $userStorage = $this->entityTypeManager->getStorage('user');
    /** @var \Drupal\Core\Session\AccountInterface|\Drupal\user\UserInterface $user */
    $user = $userStorage->load(2);
    $user->passRaw = $pass;

    $this->drupalLogin($user);
  }

}
