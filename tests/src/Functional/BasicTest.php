<?php

namespace Drupal\Tests\content_snapshot\Functional;

use Drupal\content_snapshot\SnapshotWriter\SnapshotWriter;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Class BasicTest.
 *
 * This class tests basic commands.
 *
 * @coversDefaultClass \Drupal\content_snapshot\Commands\Commands
 *
 * @group content_snapshot
 */
class BasicTest extends AbstractFunctional {

  protected static $modules = [
    'node',
    'content_snapshot',
    'language',
    'content_translation',
  ];

  /**
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpSnapshotPath();

    $this->nodeStorage = $this->entityTypeManager->getStorage('node');

    $this->createContentType([
      'type' => 'page',
      'name' => 'Page type',
    ]);
  }

  /**
   * Tests if export produces valid snapshot.
   *
   * @covers ::export
   */
  public function testExport() {
    $this->createNode();

    $this->runSnapshotExport();

    $snapshotPath = $this->config->get('snapshot_path');
    self::assertDirectoryExists($snapshotPath);
    $shapshotDataPath = "$snapshotPath/" . SnapshotWriter::DATA_DIR;
    self::assertDirectoryExists($shapshotDataPath);
    $snapshotFilesPath = "$snapshotPath/files";
    self::assertDirectoryExists($snapshotFilesPath);

    $this->validateSnapshot($snapshotPath);
  }

  /**
   * Test if purge happens automatically before the import.
   *
   * @depends testExport
   *
   * @covers ::import
   */
  public function testImportPurge() {
    $this->createNode();

    $this->runSnapshotExport();

    // Create additional node.
    $id = $this->createNode()->id();

    $this->runSnapshotImport();

    // Check if additional node has been removed.
    self::assertNull($this->nodeStorage->load($id));
  }

  /**
   * Tests if there are no changes introduced in the snapshot after the import
   * export import cycle.
   *
   * @covers ::import
   * @covers ::export
   */
  public function testExportImportExportCycle() {
    $this->createNode();
    $this->assertSnapshotConsistency();
  }

  /**
   * Tests ir purge correctly purges everything.
   *
   * @covers ::purge
   */
  public function testPurge() {
    $this->createNode();

    $this->drush('-y content-snapshot:purge');
    $nodes = $this->nodeStorage->loadMultiple();
    self::assertEmpty($nodes);
  }

  /**
   * Tests export-import cycle of the basic node properties.
   *
   * @depends testExport
   * @depends testImportPurge
   *
   * @covers ::import
   */
  public function testTitleAndBody() {
    $oldNode = $this->createNode();
    $oldBody = $oldNode->get('body')->first()->getValue()['value'];
    $oldTitle = $oldNode->getTitle();

    $this->runSnapshotExportImport();

    // Check if data in database didn't change.
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load(1);
    $body = $node->get('body')->first()->getValue()['value'];
    $this->assertEquals($body, $oldBody);
    $this->assertEquals($node->getTitle(), $oldTitle);
  }

  /**
   * @depends testTitleAndBody
   *
   * @covers ::import
   */
  public function testTitleAndBodyTranslation() {

    $translationLangcode = 'pl';

    // Create language.
    $language = ConfigurableLanguage::createFromLangcode($translationLangcode);
    $language->enable();
    $language->save();

    // Enable menu link translation.
    /** @var \Drupal\content_translation\ContentTranslationManagerInterface $manager */
    $manager = $this->container->get('content_translation.manager');
    $manager->setEnabled('node', 'page', TRUE);

    // Make "body" field translatable
    $fieldDefinitions = $this->container->get('entity_field.manager')->getFieldStorageDefinitions('node');
    /** @var \Drupal\field\FieldStorageConfigInterface $bodyFieldStorageConfig */
    $bodyFieldStorageConfig = $fieldDefinitions['body'];
    $bodyFieldStorageConfig->setTranslatable(TRUE);
    $bodyFieldStorageConfig->save();

    $oldNode = $this->createNode();
    $oldNodeTranslationBody = $this->randomMachineName(32);
    $oldNodeTranslationTitle = $this->randomMachineName(8);
    $oldNodeTranslation = $oldNode->addTranslation($translationLangcode, [
      'title' => $oldNodeTranslationTitle,
      'body' => [
        'value' => $oldNodeTranslationBody,
        'format' => filter_default_format(),
      ],
    ]);
    $oldNodeTranslation->save();

    $this->runSnapshotExportImport();

    // Check if data in database didn't change.
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load(1);

    self::assertTrue($node->hasTranslation($translationLangcode));
    $nodeTranslation = $node->getTranslation($translationLangcode);

    $body = $nodeTranslation->get('body')->first()->getValue()['value'];
    $this->assertEquals($body, $oldNodeTranslationBody);
    $this->assertEquals($nodeTranslation->getTitle(), $oldNodeTranslationTitle);
  }

}
