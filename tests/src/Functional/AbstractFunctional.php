<?php

/**
 * @file
 */

namespace Drupal\Tests\content_snapshot\Functional;

use Drupal\content_snapshot\SnapshotWriter\SnapshotWriter;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Tests\BrowserTestBase;
use Drush\TestTraits\DrushTestTrait;

/**
 * Class AbstractFunctional.
 */
abstract class AbstractFunctional extends BrowserTestBase {

  use DrushTestTrait;

  protected $defaultTheme = 'stark';

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $configFactory */
    $configFactory = $this->container->get('config.factory');
    $this->config = $configFactory->getEditable('content_snapshot.settings');
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->fileSystem = $this->container->get('file_system');

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $configFactory */
    $configFactory = $this->container->get('config.factory');
    $this->config = $configFactory->getEditable('content_snapshot.settings');
  }

  /**
   * Helper method for preparing snapshot path.
   */
  protected function prepareSnapshotPath() {
    $tempDirectory = $this->fileSystem->getTempDirectory();
    $snapshotPath = "$tempDirectory/snapshot";

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = $this->fileSystem;

    $fileSystem->prepareDirectory($snapshotPath, FileSystemInterface::CREATE_DIRECTORY);
    $fileSystem->saveData('', $fileSystem->createFilename('.snapshot', $snapshotPath));

    return $snapshotPath;
  }

  /**
   * Helper method for setting up snapshot path and saving it in configuration.
   */
  protected function setUpSnapshotPath() {
    $snapshotPath = $this->prepareSnapshotPath();
    $this->config->set('snapshot_path', $snapshotPath);
    $this->config->save();
  }

  /**
   * Helper method for validating snapshot.
   *
   * @param $snapshotPath
   */
  protected function validateSnapshot($snapshotPath) {

    $shapshotDataPath = "$snapshotPath/" . SnapshotWriter::DATA_DIR;
    $snapshotFilesPath = "$snapshotPath/files";

    $fileSystem = $this->fileSystem;
    $results = $fileSystem->scanDirectory($shapshotDataPath, '/.*/');
    // Check if produced snapshot items are valid yaml files.
    foreach ($results as $path => $result) {
      try {
        Yaml::decode(file_get_contents($path));
      } catch (\Exception $e) {
        self::assertTrue(FALSE, "Checks if snapshot item is a valid YAML file.");
      }
    }
  }

  /**
   * Helper method for making sure that snapshot doesn't change after
   * export-import-export cycle.
   */
  protected function assertSnapshotConsistency() {
    $this->drush('-y content-snapshot:export');

    $snapshotPath = $this->config->get('snapshot_path');
    $shapshotDataPath = "$snapshotPath/" . SnapshotWriter::DATA_DIR;
    $snapshotFilesPath = "$snapshotPath/files";

    $fileSystem = $this->fileSystem;
    $results = $fileSystem->scanDirectory($shapshotDataPath, '/.*/');

    $contents = [];
    foreach ($results as $path => $result) {
      $contents[$path] = file_get_contents($path);
    }

    $this->runSnapshotImport();
    $this->runSnapshotExport();

    $results = $fileSystem->scanDirectory($shapshotDataPath, '/.*/');
    foreach ($results as $path => $result) {
      self::assertEquals($contents[$path], file_get_contents($path));
    }
  }

  /**
   * Helper method.
   */
  protected function runSnapshotExportImport(string $snapshotPath = NULL) {
    $this->runSnapshotExport($snapshotPath);
    $this->runSnapshotImport($snapshotPath);
  }


  /**
   * Helper method.
   */
  protected function runSnapshotExport(string $snapshotPath = NULL) {
    $options = '';

    if ($snapshotPath) {
      $options .= "--snapshot-path=$snapshotPath";
    }

    $this->drush("-y content-snapshot:export $options");
  }

  /**
   * Helper method.
   */
  protected function runSnapshotImport(string $snapshotPath = NULL) {
    $options = '';

    if ($snapshotPath) {
      $options .= "--snapshot-path=$snapshotPath";
    }

    $this->drush("-y content-snapshot:import $options");
  }

}
