<?php

namespace Drupal\Tests\content_snapshot\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\menu_link_content\MenuLinkContentStorageInterface;
use Drush\TestTraits\DrushTestTrait;

/**
 * Class MenusTest.
 *
 * This class tests basic commands.
 *
 * @group content_snapshot
 */
class MenusTest extends AbstractFunctional {

  use DrushTestTrait;

  protected static $modules = [
    'content_snapshot',
    'menu_link_content',
    'language',
    'content_translation',
  ];

  /**
   * @var MenuLinkContentStorageInterface
   */
  private $menuLinkContentStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpSnapshotPath();

    $this->menuLinkContentStorage = $this->entityTypeManager->getStorage('menu_link_content');

    // Create menu.
    /** @var \Drupal\system\MenuInterface $menu */
    $menu = $this->entityTypeManager->getStorage('menu')
    ->create([
      'langcode' => 'en',
      'status' => TRUE,
      'id' => 'test-menu',
      'label' => 'Test Menu',
    ]);
    $menu->save();
  }

  /**
   * Tests export-import cycle of the menu links.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testLinksStructure() {

    $menuArray = $this->getMenuArray();
    $this->createMenuItems($menuArray);

    $this->runSnapshotExportImport();

    $this->assertMenuItems($menuArray);
  }

  /**
   * Tests export-import cycle of the menu links.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @covers \Drupal\content_snapshot\EventSubscriber\MenusHierarchyFixerEventSubscriber::postImport
   */
  public function testMenuTree() {
    $menuArray = $this->getMenuArray();
    $this->createMenuItems($menuArray);

    $this->runSnapshotExportImport();

    /** @var \Drupal\Core\Menu\MenuLinkManagerInterface $menuLinkManager */
    $menuLinkManager = $this->container->get('plugin.manager.menu.link');

    // Make sure that menu_tree is correct after the export-import
    // cycle.
    $childIds = $menuLinkManager->getChildIds('menu_link_content:' . $menuArray[0]['uuid']);
    $this->assertEquals(count($childIds), count($menuArray[0]['children']));
  }

  /**
   * Tests export-import cycle of the menu links.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testMenuTranslation() {

    $translationLangcode = 'pl';

    // Create language.
    $language = ConfigurableLanguage::createFromLangcode($translationLangcode);
    $language->enable();
    $language->save();

    // Enable menu link translation.
    /** @var \Drupal\content_translation\ContentTranslationManagerInterface $manager */
    $manager = $this->container->get('content_translation.manager');
    $manager->setEnabled('menu_link_content', 'menu_link_content', TRUE);

    $menuArray = $this->getMenuArray();
    $this->createMenuItems($menuArray, $translationLangcode);

    $this->runSnapshotExportImport();

    $this->assertTranslation($menuArray, $translationLangcode);
  }

  /**
   * Helper function.
   *
   * @param array $items
   * @param string $translation
   */
  private function assertTranslation(array $items, string $translation) {
    foreach ($items as $item) {

      /** @var \Drupal\menu_link_content\MenuLinkContentInterface $menuItem */
      $menuItem = $this->menuLinkContentStorage->load($item['id']);
      self::assertNotNull($menuItem);

      if (!$menuItem) {
        continue;
      }

      // Check if menu item still has translation.
      self::assertTrue($menuItem->hasTranslation($translation));

      if ($menuItem->hasTranslation($translation)) {
        $menuItemTranslation = $menuItem->getTranslation($translation);
        $this->assertEquals($menuItemTranslation->getTitle(), $item['title'][$translation]);
      }

      if (!empty($item['children'])) {
        $this->assertTranslation($item['children'], $translation);
      }
    }
  }

  /**
   * Helper method.
   *
   * @param array $items
   */
  private function assertMenuItems(array $items) {
    foreach ($items as $item) {
      /** @var \Drupal\menu_link_content\MenuLinkContentInterface $menuItem */
      $menuItem = $this->menuLinkContentStorage->load($item['id']);
      self::assertNotNull($menuItem);

      if (!$menuItem) {
        continue;
      }

      if (!empty($item['parent'])) {
        $this->assertEquals($menuItem->getParentId(), $item['parent']);
      }

      $this->assertEquals($menuItem->uuid(), $item['uuid']);
      $this->assertEquals($menuItem->getUrlObject()->toUriString(), $item['uri']);
      $this->assertEquals($menuItem->getTitle(), $item['title']['en']);
      self::assertTrue($menuItem->isExpanded());
      $this->assertEquals($menuItem->getWeight(), $item['weight']);

      if (!empty($item['children'])) {
        $this->assertMenuItems($item['children']);
      }
    }
  }

  /**
   * @param array $menuArray
   * @param string|false $translationLangcode
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createMenuItems(array $menuArray, $translationLangcode = FALSE) {
    foreach($menuArray as $item) {
      $id = $item['id'];
      $uuid = $item['uuid'];
      $uri = $item['uri'];
      $title = $item['title'];
      $weight = $item['weight'];

      $values = [
        'id' => $id,
        'uuid' => $uuid,
        'title' => $title['en'],
        'link' => ['uri' => $uri],
        'menu_name' => 'test-menu',
        'expanded' => TRUE,
        'weight' => $weight,
      ];

      if (!empty($item['parent'])) {
        $values['parent'] = $item['parent'];
      }

      $parent = MenuLinkContent::create($values);
      $parent->save();

      if ($translationLangcode) {
        // Create Polish translation
        $parentTranslation = $parent->addTranslation($translationLangcode, [
          'title' => $title[$translationLangcode],
        ]);
        $parentTranslation->save();
      }

      if (!empty($item['children'])) {
        $this->createMenuItems($item['children'], $translationLangcode);
      }
    }
  }

  private function getMenuArray() {
    /** @var \Drupal\Component\Uuid\UuidInterface $uuidGenerator */
    $uuidGenerator = $this->container->get('uuid');

    return [
      [
        'uuid' => $parent = $uuidGenerator->generate(),
         // High id for the parent item is intentional. It's important for the
         // later menu tree testing.
        'id' => 4,
        'uri' => 'route:<front>',
        'title' => [
          'en' => 'Front #1',
          'pl' => 'Front pl #1',
        ],
        'weight' => 0,
        'children' => [
          [
            'uuid' => $uuidGenerator->generate(),
            'id' => 2,
            'uri' => 'route:<front>',
            'title' => [
              'en' => 'Front #2',
              'pl' => 'Front pl #2',
            ],
            'weight' => 0,
            'parent' => 'menu_link_content:' . $parent,
          ],
          [
            'uuid' => $uuidGenerator->generate(),
            'id' => 3,
            'uri' => 'route:<front>',
            'title' => [
              'en' => 'Front #3',
              'pl' => 'Front pl #3',
            ],
            'weight' => 1,
            'parent' => 'menu_link_content:' . $parent,
          ],
          [
            'uuid' => $uuidGenerator->generate(),
            'id' => 1,
            'uri' => 'route:<nolink>',
            'title' => [
              'en' => 'No link',
              'pl' => 'No link pl',
            ],
            'weight' => 2,
            'parent' => 'menu_link_content:' . $parent,
          ],
        ],
      ],
    ];
  }

}
