<?php

/**
 * @file
 */

namespace Drupal\Tests\content_snapshot\Functional;

/**
 * Class OverrideTest.
 *
 * @coversDefaultClass \Drupal\content_snapshot\Commands\Commands
 *
 * @group content_snapshot
 */
class OverrideTest extends AbstractFunctional {

  protected static $modules = [
    'node',
    'content_snapshot',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->nodeStorage = $this->entityTypeManager->getStorage('node');

    $this->createContentType([
      'type' => 'page',
      'name' => 'Page type',
    ]);
  }

  /**
   * Tests if export produces valid snapshot.
   *
   * @covers ::export
   */
  public function testExportSnapshotPathOverride() {
    $this->createNode();

    // We are preparing new snapshot_path but are not saving it in
    // configuration.
    $snapshotPath = $this->prepareSnapshotPath();

    $this->drush('-y content-snapshot:export --snapshot-path=' . $snapshotPath);

    $this->validateSnapshot($snapshotPath);
  }

  /**
   * Tests if export produces valid snapshot.
   *
   * @covers ::import
   */
  public function testImportSnapshotPathOverride() {
    $node = $this->createNode();
    $title = $node->getTitle();

    // We are preparing new snapshot_path but are not saving it in
    // configuration.
    $snapshotPath = $this->prepareSnapshotPath();

    $this->runSnapshotExportImport($snapshotPath);

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load(1);
    $this->assertEquals($node->getTitle(), $title);
  }

}
