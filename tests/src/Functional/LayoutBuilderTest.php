<?php

/**
 * @file
 */

namespace Drupal\Tests\content_snapshot\Functional;

use Drupal\block_content\BlockContentInterface;
use Drupal\block_content\Entity\BlockContent;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\Tests\content_snapshot\Traits\BlockContentTrait;

/**
 * Class LayoutBuilderTest.
 *
 * This class tests layout_builder module handling.
 *
 * @coversDefaultClass \Drupal\content_snapshot\Commands\Commands
 *
 * @group content_snapshot
 */
class LayoutBuilderTest extends AbstractFunctional {

  use BlockContentTrait;

  protected static $modules = [
    'node',
    'content_snapshot',
    'block_content',
    'layout_builder',
  ];

  /**
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * @var \Drupal\Component\Uuid\Uuid
   */
  protected $uuidGenerator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpSnapshotPath();

    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->uuidGenerator = $this->container->get('uuid');

    $this->createContentType([
      'type' => 'page',
      'name' => 'Page type',
    ]);

    $this->createBlockContentType('basic', [], TRUE);

    // Enable layout builder overrides.
    LayoutBuilderEntityViewDisplay::load('node.page.default')
      ->enableLayoutBuilder()
      ->setOverridable()
      ->save();
  }

  /**
   * Tests if paragraphs export import export produces consistent snapshots.
   */
  public function testExportImportExportCycle() {
    $this->createNodeWithLayout();
    $this->assertSnapshotConsistency();
  }

  /**
   * Tests if custom block reference is stored correctly.
   */
  public function testInlineBlockReference() {
    $node = $this->createNodeWithLayout();

    $this->runSnapshotExportImport();

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load($node->id());
    $layoutBuilderLayout = $node->get('layout_builder__layout')->first()->getValue();

    /** @var Section $section */
    $section = $layoutBuilderLayout['section'];
    $components = $section->getComponents();

    /** @var SectionComponent $component */
    $component = reset($components);
    $componentArray = $component->toArray();
    $blockConfiguration = $componentArray['configuration'];

    $this->assertEquals($node->getRevisionId(), $blockConfiguration['block_revision_id']);
  }


  /**
   * Helper method.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createNodeWithLayout() {

    $blockSectionComponent = new SectionComponent($this->uuidGenerator->generate(), 'content', $this->getInlineBlockConfiguration($this->createBlockContent()));

    return $this->drupalCreateNode([
      'layout_builder__layout' => [
        new Section('layout_onecol', [], [
          $blockSectionComponent,
        ]),
      ],
    ]);
  }

  /**
   * Helper method.
   *
   * @return \Drupal\block_content\Entity\BlockContent|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createBlockContent() {
    $blockContent = BlockContent::create([
      'type' => 'basic',
      'info' => $this->randomString(),
      'reusable' => 0,
      'body' => [
        'format' => 'full_html',
        'value' => $this->randomGenerator->paragraphs(),
      ],
    ]);
    $blockContent->save();

    return $blockContent;
  }

  /**
   * Helper method.
   *
   * @param \Drupal\block_content\BlockContentInterface $blockContent
   *
   * @return array
   */
  private function getInlineBlockConfiguration(BlockContentInterface $blockContent): array {
    return [
      'id' => 'inline_block:' . $blockContent->bundle(),
      'label' => $blockContent->label(),
      'provider' => 'layout_builder',
      'label_display' => 'visible',
      'view_mode' => 'full',
      'block_revision_id' => $blockContent->getRevisionId(),
      'block_serialized' => NULL,
      'context_mapping' => [],
    ];
  }

}
