<?php

/**
 * @file
 */

namespace Drupal\Tests\content_snapshot\Traits;

use Drupal\block_content\Entity\BlockContentType;

/**
 * Trait BlockContentTrait.
 */
trait BlockContentTrait {

  /**
   * Creates a custom block type (bundle).
   *
   * @param array|string $values
   *   The value to create the block content type. If $values is an array
   *   it should be like: ['id' => 'foo', 'label' => 'Foo']. If $values
   *   is a string, it will be considered that it represents the label.
   * @param bool $create_body
   *   Whether or not to create the body field
   *
   * @return \Drupal\block_content\Entity\BlockContentType
   *   Created custom block type.
   */
  private function createBlockContentType(string $id, array $values, $create_body = FALSE) {

    if (is_array($values)) {
      $values += [
        'id' => $id,
        'label' => $id,
        'revision' => FALSE,
      ];
      $bundle = BlockContentType::create($values);
    }
    else {
      $bundle = BlockContentType::create([
        'id' => $id,
        'label' => $values,
        'revision' => FALSE,
      ]);
    }
    $bundle->save();
    if ($create_body) {
      block_content_add_body_field($bundle->id());
    }
    return $bundle;
  }

}
