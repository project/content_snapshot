<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Importer;

/**
 * Interface ImporterInterface.
 */
interface ImporterInterface {

  public const PROCESSING_PHASE_CONVERTING = 'converting';
  public const PROCESSING_PHASE_INITIAL_IMPORT = 'initial_import';
  public const PROCESSING_PHASE_RESTORING_REFERENCES = 'restoring_references';
  public const PROCESSING_PHASE_FINISHING = 'finishing';

  /**
   * @return void
   */
  public function import(): void;
}
