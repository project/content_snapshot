<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Importer;

use Drupal\content_snapshot\ContentLoader\ContentLoaderInterface;
use Drupal\content_snapshot\Converter\ToObjectConverterInterface;
use Drupal\content_snapshot\Converter\ToSnapshotItemConverterInterface;
use Drupal\content_snapshot\Event\SnapshotEvent;
use Drupal\content_snapshot\FilesRestorer\FilesRestorerInterface;
use Drupal\content_snapshot\ReferenceRestorer\ReferenceRestorerInterface;
use Drupal\content_snapshot\ContentWriter\ContentWriterInterface;
use Drupal\content_snapshot\Purger\PurgerInterface;
use Drupal\content_snapshot\SnapshotReader\SnapshotReaderInterface;
use Drupal\content_snapshot\Event\SnapshotItemEvent;
use Drupal\content_snapshot\StreamWrapper\ContentSnapshotStream;
use Drupal\content_snapshot\Utils\FileSystemUtilsInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class Importer.
 */
class Importer implements ImporterInterface {

  /**
   * @var \Drupal\content_snapshot\Purger\PurgerInterface
   */
  private $purger;

  /**
   * @var \Drupal\content_snapshot\SnapshotReader\SnapshotReaderInterface
   */
  private $snapshotReader;

  /**
   * @var \Drupal\content_snapshot\ContentWriter\ContentWriterInterface
   */
  private $contentWriter;

  /**
   * @var \Drupal\content_snapshot\ContentLoader\ContentLoaderInterface
   */
  private $contentLoader;

  /**
   * @var \Drupal\content_snapshot\ReferenceRestorer\ReferenceRestorerInterface
   */
  private $referenceRestorer;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * @var \Drupal\content_snapshot\FilesRestorer\FilesRestorerInterface
   */
  private $filesRestorer;

  /**
   * @var \Drupal\content_snapshot\Converter\ToSnapshotItemConverterInterface
   */
  private $toSnapshotItemConverter;

  /**
   * @var \Drupal\content_snapshot\Converter\ToObjectConverterInterface
   */
  private $toObjectConverter;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * @var \Drupal\content_snapshot\Utils\FileSystemUtilsInterface
   */
  private $fileSystemUtils;

  /**
   * Importer constructor.
   *
   * @param \Drupal\content_snapshot\Purger\PurgerInterface $purger
   * @param \Drupal\content_snapshot\SnapshotReader\SnapshotReaderInterface $snapshotReader
   * @param \Drupal\content_snapshot\Converter\ToSnapshotItemConverterInterface $toSnapshotItemConverter
   * @param \Drupal\content_snapshot\Converter\ToObjectConverterInterface $toObjectConverter
   * @param \Drupal\content_snapshot\ContentWriter\ContentWriterInterface $contentWriter
   * @param \Drupal\content_snapshot\ContentLoader\ContentLoaderInterface $contentLoader
   * @param \Drupal\content_snapshot\ReferenceRestorer\ReferenceRestorerInterface $referenceRestorer
   * @param \Drupal\content_snapshot\FilesRestorer\FilesRestorerInterface $filesRestorer
   * @param \Psr\Log\LoggerInterface $logger
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   * @param \Drupal\content_snapshot\Utils\FileSystemUtilsInterface $fileSystemUtils
   */
  public function __construct(
    PurgerInterface $purger,
    SnapshotReaderInterface $snapshotReader,
    ToSnapshotItemConverterInterface $toSnapshotItemConverter,
    ToObjectConverterInterface $toObjectConverter,
    ContentWriterInterface $contentWriter,
    ContentLoaderInterface $contentLoader,
    ReferenceRestorerInterface $referenceRestorer,
    FilesRestorerInterface $filesRestorer,
    LoggerInterface $logger,
    EventDispatcherInterface $eventDispatcher,
    FileSystemUtilsInterface $fileSystemUtils
  ) {
    $this->purger = $purger;
    $this->snapshotReader = $snapshotReader;
    $this->toSnapshotItemConverter = $toSnapshotItemConverter;
    $this->toObjectConverter = $toObjectConverter;
    $this->contentWriter = $contentWriter;
    $this->contentLoader = $contentLoader;
    $this->referenceRestorer = $referenceRestorer;
    $this->filesRestorer = $filesRestorer;
    $this->logger = $logger;
    $this->eventDispatcher = $eventDispatcher;
    $this->fileSystemUtils = $fileSystemUtils;
  }

  /**
   * {@inheritDoc}
   */
  public function import(): void {

    $this->logger->info("Removing existing content.");
    $this->purger->purge();

    $this->logger->info("Restoring files.");

    $streams = $this->fileSystemUtils->getSubdirectories(ContentSnapshotStream::STREAM_PATH . "files");
    foreach ($streams as $stream) {
      $this->filesRestorer->restoreStream($stream);
    }

    $this->logger->info("Content import started.");
    $this->logger->info("Loading snapshot files.");
    // Import content without resolving references.
    $snapshotArray = $this->snapshotReader->read();
    $snapshotItems = [];

    // Produce snapshot objects.
    $this->logger->info("Producing snapshot objects.");
    foreach ($snapshotArray as $snapshotItemArray) {
      $snapshotItem = $this->toSnapshotItemConverter->arrayToSnapshot($snapshotItemArray);

      if (!$snapshotItem) {
        throw new \LogicException("Content array couldn't be converted into snapshot item.");
      }

      $this->eventDispatcher->dispatch(SnapshotItemEvent::IMPORT_POST_CONVERTING, new SnapshotItemEvent($snapshotItem));

      if ($snapshotItem->isExcluded()) {
        continue;
      }

      $snapshotItems[] = $snapshotItem;
    }

    usort($snapshotItems, static function($a, $b) {
        return  $b->getPriority() <=> $a->getPriority();
    });

    // Items here are already individually pre-sorted, but can be altered further.
    $this->eventDispatcher->dispatch(SnapshotEvent::IMPORT_POST_CONVERTING, new SnapshotEvent($snapshotItems));

    // Import snapshot objects.
    foreach ($snapshotItems as $snapshotItem) {
      $this->logger->info("Importing: " . $snapshotItem->getPath() . DIRECTORY_SEPARATOR . $snapshotItem->getId());

      $object = $this->toObjectConverter->snapshotItemToObject($snapshotItem);
      if (!$object) {
        throw new \LogicException("Snapshot couldn't be converted into the actual content object.");
      }

      if (!$this->contentWriter->write($object)) {
        throw new \LogicException("Content couldn't be written to the database.");
      }

      $this->eventDispatcher->dispatch(SnapshotItemEvent::IMPORT_POST_INITIAL_IMPORT, new SnapshotItemEvent($snapshotItem));
    }

    $this->eventDispatcher->dispatch(SnapshotEvent::IMPORT_POST_INITIAL_IMPORT, new SnapshotEvent($snapshotItems));

    // References restoring phase.
    $this->logger->info("References restoring started.");
    foreach ($snapshotItems as $filename => $snapshotItem) {
      $object = $this->contentLoader->load($snapshotItem);
      if (!$object) {
        throw new \LogicException("Object has not been found in the database.");
      }

      $hasReferences = $this->referenceRestorer->restoreReferences($snapshotItem, $object);
      if ($hasReferences) {
        $this->logger->info("Restoring references: " . $snapshotItem->getPath() . DIRECTORY_SEPARATOR . $snapshotItem->getId());

        if (!$this->contentWriter->write($object)) {
          throw new \LogicException("Content couldn't be written to the database.");
        }

        $this->eventDispatcher->dispatch(SnapshotItemEvent::IMPORT_POST_REFERENCE_RESTORING, new SnapshotItemEvent($snapshotItem));
      }
    }

    $this->eventDispatcher->dispatch(SnapshotEvent::IMPORT_POST_REFERENCE_RESTORING, new SnapshotEvent($snapshotItems));

    $this->logger->info("Post importing work started.");
    // Now that we have everything imported. We can do some further
    // post-processing if it's needed.
    foreach ($snapshotItems as $filename => $snapshotItem) {
      $this->eventDispatcher->dispatch(SnapshotItemEvent::IMPORT_POST_FINISHING, new SnapshotItemEvent($snapshotItem));
    }

    $this->eventDispatcher->dispatch(SnapshotEvent::IMPORT_POST_FINISHING, new SnapshotEvent($snapshotItems));

    $this->logger->info("Import is complete.");
  }

}
