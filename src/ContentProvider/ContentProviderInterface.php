<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ContentProvider;

/**
 * Interface ContentProviderInterface.
 */
interface ContentProviderInterface {

  /**
   * @return array
   */
  public function provide(): array;

}
