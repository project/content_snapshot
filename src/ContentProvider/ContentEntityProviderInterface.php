<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ContentProvider;

/**
 * Interface ContentProviderInterface.
 */
interface ContentEntityProviderInterface extends ContentProviderInterface {

  /**
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function provide(): array;

}
