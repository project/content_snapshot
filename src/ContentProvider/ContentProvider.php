<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ContentProvider;

/**
 * Class ContentProvider.
 */
class ContentProvider implements ContentProviderInterface {

  /**
   * @var \Drupal\content_snapshot\ContentProvider\ContentProviderInterface[]
   */
  private $providers = [];

  /**
   * @param \Drupal\content_snapshot\ContentProvider\ContentProviderInterface $provider
   */
  public function addProvider(ContentProviderInterface $provider) {
    $this->providers[] = $provider;
  }

  /**
   * {@inheritDoc}
   */
  public function provide(): array {
    $content = [];
    foreach ($this->providers as $provider) {
      $content = array_merge($content, $provider->provide());
    }

    return $content;
  }
}
