<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ContentProvider;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class ContentEntityProvider.
 */
class ContentEntityProvider implements ContentEntityProviderInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * ContentEntityProvider constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function provide(): array {
    $entities = [];

    $entityTypeDefinations = $this->entityTypeManager->getDefinitions();
    foreach ($entityTypeDefinations as $entityTypeId => $definition) {
      if ($definition->entityClassImplements(ContentEntityInterface::class)) {
        $loaded = $this->entityTypeManager->getStorage($entityTypeId)->loadMultiple();

        if ($entityTypeId === 'user') {
          // Do not export anonymous user and admin.
          unset($loaded[0]);
        }

        $entities = array_merge($entities, $loaded);
      }
    }

    return $entities;
  }

}
