<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\SnapshotWriter;

use Drupal\content_snapshot\Configuration\ConfigInterface;
use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\content_snapshot\Logger\LoggerInterface;
use Drupal\content_snapshot\StreamWrapper\ContentSnapshotStream;
use Drupal\content_snapshot\Utils\FileSystemUtilsInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Class SnapshotWriter.
 */
class SnapshotWriter implements SnapshotWriterInterface {

  public const DATA_DIR = 'data';

  /**
   * @var \Drupal\content_snapshot\Configuration\ConfigInterface
   */
  private $config;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * @var \Drupal\content_snapshot\Encoder\YamlEncoder
   */
  private $yamlEncoder;

  /**
   * @var string
   */
  private $targetPath = ContentSnapshotStream::STREAM_PATH;

  /**
   * @var \Drupal\content_snapshot\Utils\FileSystemUtilsInterface
   */
  private $fileSystemUtils;

  /**
   * @var \Drupal\content_snapshot\Logger\LoggerInterface
   */
  private $logger;

  /**
   * SnapshotWriter constructor.
   *
   * @param \Drupal\content_snapshot\Configuration\ConfigInterface $config
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \Drupal\content_snapshot\Encoder\YamlEncoder $yamlEncoder
   * @param \Drupal\content_snapshot\Utils\FileSystemUtilsInterface $fileSystemUtils
   * @param \Drupal\content_snapshot\Logger\LoggerInterface $logger
   */
  public function __construct(
    ConfigInterface          $config,
    FileSystemInterface      $fileSystem,
    YamlEncoder              $yamlEncoder,
    FileSystemUtilsInterface $fileSystemUtils,
    LoggerInterface          $logger
  ) {
    $this->config = $config;
    $this->fileSystem = $fileSystem;
    $this->yamlEncoder = $yamlEncoder;
    $this->fileSystemUtils = $fileSystemUtils;
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public function purge(): void {
    $this->validateSnapshotPath();

    $scheme = $this->targetPath;

    if (!file_exists("$scheme.snapshot")) {
      $snapshotPath = $this->config->getSnapshotPath();
      throw new \LogicException("'.snapshot' file not found in '$snapshotPath' directory. You need to create an empty '.snapshot' file there. Selected directory is going to be purged before the export, '.snapshot' file is a safeguard against accidentally purging a wrong directory.");
    }

    $this->fileSystemUtils->purgeDirectory($scheme);
  }

  /**
   * {@inheritDoc}
   */
  public function writeContentItem(string $id, array $content, string $path) {
    $this->validateSnapshotPath();

    $scheme = $this->targetPath;

    $encoded = $this->yamlEncoder->encode($content, YamlEncoder::FORMAT);
    $destinationDirectory = $scheme . self::DATA_DIR . DIRECTORY_SEPARATOR . $path;
    $this->fileSystem->prepareDirectory($destinationDirectory, FileSystemInterface::CREATE_DIRECTORY);
    $destinationFile = $destinationDirectory . DIRECTORY_SEPARATOR . $id . '.yml';
    $this->logger->info("Saving: $destinationFile");
    $this->fileSystem->saveData($encoded, $destinationFile);
  }

  /**
   * {@inheritDoc}
   */
  public function writeStream(string $scheme, ?string $path_exclude_pattern): void {
    $this->validateSnapshotPath();

    $destination = $this->targetPath . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $scheme;

    if (!$this->fileSystem->realpath("$scheme://")) {
      $streamsConfig = $this->config->getSnapshotStreams();
      $streamConfig = current(array_filter($streamsConfig, static function($element) use ($scheme) { return $element['scheme'] === $scheme; }));
      if (!$streamConfig || empty($streamConfig['optional'])) {
        $this->logger->warning("'$scheme' stream is not accessible.");
        return;
      }

      return;
    }

    $this->logger->info("Snapshotting stream files: $scheme://");

    $this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY);
    $this->fileSystemUtils->copyDirectory("$scheme://", $destination, (string) $path_exclude_pattern);
  }

  /**
   * Helper method for validating if snapshot_path is correct.
   */
  private function validateSnapshotPath() {
    if (!$this->fileSystem->prepareDirectory($this->targetPath)) {
      $snapshotPath = $this->config->getSnapshotPath();
      throw new \LogicException("Target directory for snapshot '$snapshotPath' doesn't exist or is not writable.");
    }
  }

}
