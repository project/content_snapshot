<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\SnapshotWriter;

/**
 * Interface SnapshotWriterInterface
 */
interface SnapshotWriterInterface {

  /**
   * Purges snapshot.
   *
   * @return void
   */
  public function purge(): void;

  /**
   * @param string $id
   * @param array $content
   * @param string $path
   *
   * @return mixed
   */
  public function writeContentItem(string $id, array $content, string $path);

  /**
   * @param string $scheme
   *
   * @param string|null $pathExcludePattern
   *
   * @return void
   */
  public function writeStream(string $scheme, ?string $pathExcludePattern): void;
}
