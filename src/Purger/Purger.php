<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Purger;

/**
 * Class Purger.
 */
class Purger implements PurgerInterface {

  /**
   * @var \Drupal\content_snapshot\Purger\PurgerInterface[]
   */
  private $purgers = [];

  public function addPurger(PurgerInterface $purger) {
    $this->purgers[] = $purger;
  }

  /**
   * {@inheritDoc}
   */
  public function purge(): void {
    foreach ($this->purgers as $purger) {
      $purger->purge();
    }
  }

}
