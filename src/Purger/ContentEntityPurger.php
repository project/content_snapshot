<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Purger;

use Drupal\content_snapshot\ContentProvider\ContentEntityProviderInterface;

/**
 * Class ContentEntityPurger.
 */
class ContentEntityPurger implements PurgerInterface {

  /**
   * @var \Drupal\content_snapshot\ContentProvider\ContentProviderInterface
   */
  private $contentProvider;

  /**
   * ContentEntityPurger constructor.
   *
   * @param \Drupal\content_snapshot\ContentProvider\ContentEntityProviderInterface $contentProvider
   */
  public function __construct(ContentEntityProviderInterface $contentProvider) {
    $this->contentProvider = $contentProvider;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function purge(): void {
    $entities = $this->contentProvider->provide();
    foreach ($entities as $entity) {
      $entity->delete();
    }
  }

}
