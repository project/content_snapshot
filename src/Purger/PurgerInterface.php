<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Purger;

/**
 * Interface PurgerInterface.
 */
interface PurgerInterface {

  /**
   * Remove all content from the website.
   *
   * @return void
   */
  public function purge(): void;
}
