<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Converter;

use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\content_snapshot\Normalizer\ContentSnapshotNormalizerContextTrait;
use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Class ContentEntityConverter.
 */
class ContentEntityToObjectConverter implements ToObjectConverterInterface {

  /**
   * @var \Symfony\Component\Serializer\Normalizer\DenormalizerInterface
   */
  private $denormalizer;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  public function __construct(DenormalizerInterface $denormalizer, EntityTypeManagerInterface $entityTypeManager) {
    $this->denormalizer = $denormalizer;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public function snapshotItemToObject(SnapshotItemInterface $snapshotItem) {
    $entityTypeId = $snapshotItem->getEntityType();
    $entityType = $this->entityTypeManager->getDefinition($entityTypeId);

    if (!$entityType) {
      return FALSE;
    }

    $class = $entityType->getClass();

    $context = [
      ContentSnapshotNormalizerContextTrait::$CONTENT_SNAPSHOT_CONTEXT => TRUE,
    ];

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    return $this->denormalizer->denormalize($snapshotItem->toArray(), $class, YamlEncoder::FORMAT, $context);
  }

}
