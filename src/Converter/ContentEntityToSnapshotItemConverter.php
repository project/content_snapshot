<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Converter;

use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\content_snapshot\Normalizer\ContentSnapshotNormalizerContextTrait;
use Drupal\content_snapshot\Snapshot\ContentEntitySnapshotItem;
use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class ContentEntityToSnapshotItemConverter.
 */
class ContentEntityToSnapshotItemConverter implements ToSnapshotItemConverterInterface {

  /**
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  private $normalizer;

  public function __construct(NormalizerInterface $normalizer) {
    $this->normalizer = $normalizer;
  }

  /**
   * {@inheritDoc}
   */
  public function arrayToSnapshot(array $content): ?SnapshotItemInterface {
    return ContentEntitySnapshotItem::fromArray($content);
  }

  /**
   * {@inheritDoc}
   */
  public function objectToSnapshot($object): ?SnapshotItemInterface {

    if (!$object instanceof ContentEntityInterface) {
      return NULL;
    }

    $context = [
      ContentSnapshotNormalizerContextTrait::$CONTENT_SNAPSHOT_CONTEXT => TRUE,
    ];

    $normalized = $this->normalizer->normalize($object, YamlEncoder::FORMAT, $context);

    if (!$normalized) {
      return NULL;
    }

    return new ContentEntitySnapshotItem(
      $object->id(),
      $object->uuid(),
      $normalized,
      $object->getEntityTypeId(),
      $object->bundle(),
      $object->isTranslatable()
    );
  }

}
