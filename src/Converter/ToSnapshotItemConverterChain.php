<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Converter;

use Drupal\content_snapshot\Logger\LoggerInterface;
use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;

/**
 * Class ToSnapshotItemConverterChain.
 */
class ToSnapshotItemConverterChain implements ToSnapshotItemConverterInterface {

  /**
   * @var \Drupal\content_snapshot\Converter\ToSnapshotItemConverterInterface[]
   */
  private $converters = [];

  /**
   * @param \Drupal\content_snapshot\Converter\ToSnapshotItemConverterInterface $converter
   */
  public function addConverter(ToSnapshotItemConverterInterface $converter): void {
    $this->converters[] = $converter;
  }

  /**
   * {@inheritDoc}
   */
  public function arrayToSnapshot(array $content): ?SnapshotItemInterface {
    foreach ($this->converters as $converter) {
      if ($result = $converter->arrayToSnapshot($content)) {
        return $result;
      }
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function objectToSnapshot($object): ?SnapshotItemInterface {
    foreach ($this->converters as $converter) {
      if ($result = $converter->objectToSnapshot($object)) {
        return $result;
      }
    }

    return NULL;
  }

}
