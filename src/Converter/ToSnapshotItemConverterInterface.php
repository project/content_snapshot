<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Converter;

use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;

/**
 * Interface ToSnapshotItemConverterInterface.
 */
interface ToSnapshotItemConverterInterface {

  /**
   * @param array $content
   *
   * @return \Drupal\content_snapshot\Snapshot\SnapshotItemInterface|null
   */
  public function arrayToSnapshot(array $content): ?SnapshotItemInterface;

  /**
   * @param $object
   *
   * @return \Drupal\content_snapshot\Snapshot\SnapshotItemInterface|null
   */
  public function objectToSnapshot($object): ?SnapshotItemInterface;

}
