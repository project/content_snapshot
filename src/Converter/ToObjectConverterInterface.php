<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Converter;

use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;

/**
 * Interface ToObjectConverterInterface.
 */
interface ToObjectConverterInterface {

  /**
   * @param \Drupal\content_snapshot\Snapshot\SnapshotItemInterface $snapshotItem
   *
   * @return mixed
   */
  public function snapshotItemToObject(SnapshotItemInterface $snapshotItem);

}
