<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Converter;

use Drupal\content_snapshot\Logger\LoggerInterface;
use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;

/**
 * Class ToObjectConverterChain.
 */
class ToObjectConverterChain implements ToObjectConverterInterface {

  /**
   * @var \Drupal\content_snapshot\Converter\ToObjectConverterInterface[]
   */
  private $converters = [];

  public function addConverter(ToObjectConverterInterface $converter): void {
    $this->converters[] = $converter;
  }

  /**
   * {@inheritDoc}
   */
  public function snapshotItemToObject(SnapshotItemInterface $snapshotItem) {
    foreach ($this->converters as $converter) {
      if ($result = $converter->snapshotItemToObject($snapshotItem)) {
        return $result;
      }
    }

    return NULL;
  }

}
