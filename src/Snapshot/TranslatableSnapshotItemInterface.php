<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Snapshot;

/**
 * Interface TranslatableSnapshotItemInterface.
 */
interface TranslatableSnapshotItemInterface extends SnapshotItemInterface {

  public const TRANSLATIONS_KEY = '_content_snapshot_translations';

  /**
   * @param string $langcode
   *
   * @return array[]
   */
  public function getTranslation(string $langcode): ?array;

  /**
   * Is entity translatable?
   *
   * @return bool
   */
  public function isTranslatable(): bool;

  /**
   * Returns normalized entity fields only.
   *
   * @return array[]
   */
  public function getNormalizedFields(): array;
}
