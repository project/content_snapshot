<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Snapshot;

/**
 * Interface EntitySnapshotInterface.
 */
interface EntitySnapshotItemInterface extends SnapshotItemInterface {

  /**
   * @return string
   */
  public function getEntityType(): string;

  /**
   * @return string
   */
  public function getBundle(): string;

}
