<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Snapshot;

/**
 * Interface SnapshotItemInterface.
 *
 * Piece of content ready to be stored as a snapshot.
 */
interface SnapshotItemInterface {

  /**
   * This has to be a unique id.
   *
   * @return string
   */
  public function getId(): string;

  /**
   * Item's UUID.
   *
   * @return string|null
   */
  public function getUuid(): ?string;

  /**
   * Path in the snapshot structure.
   *
   * @return string
   */
  public function getPath(): ?string;

  /**
   * Returns content just how it was originally normalized.
   *
   * @return array
   */
  public function getNormalizedContent(): array;

  /**
   * Sets priority used for ordering of items during the processing.
   *
   * @param int $priority
   *
   * @return mixed
   */
  public function setPriority(int $priority);

  /**
   * Returns priority used in ordering.
   *
   * @return int
   */
  public function getPriority(): int;

  /**
   * Tells if item is excluded from the further processing.
   *
   * @return bool
   */
  public function isExcluded(): bool;

  /**
   * @param bool $bool
   */
  public function setExcluded(bool $bool): void;

  /**
   * Returns normalized content and additional metadata.
   *
   * @return array
   */
  public function toArray(): array;

}
