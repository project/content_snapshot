<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Snapshot;

/**
 * Class SnapshotItem.
 */
class SnapshotItem implements SnapshotItemInterface {

  /**
   * @var string
   */
  protected const TYPE = 'generic_content';

  /**
   * Key under which content's metadata should be stored in snapshot array.
   */
  private const METADATA_KEY = '_content_snapshot_metadata';

  /**
   * @var string
   */
  private $id;

  /**
   * @var array
   */
  private $data;

  /**
   * @var string
   */
  private $getUuid;

  /**
   * @var int
   */
  private $priority = 0;

  /**
   * @var bool
   */
  private $excluded = FALSE;

  /**
   * SnapshotItem constructor.
   *
   * @param string $id
   * @param string $getUuid
   * @param array $data
   */
  public function __construct(string $id, string $getUuid, array $data) {
    $this->id = $id;
    $this->getUuid = $getUuid;
    $this->data = $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * {@inheritDoc}
   */
  public function getUuid(): string {
    return $this->getUuid;
  }

  /**
   * {@inheritDoc}
   */
  public function getPath(): ?string {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getNormalizedContent(): array {
    return $this->data;
  }

  /**
   * {@inheritDoc}
   */
  public function setPriority(int $priority) {
    $this->priority = $priority;
  }

  /**
   * {@inheritDoc}
   */
  public function getPriority(): int {
    return $this->priority;
  }

  /**
   * {@inheritDoc}
   */
  public function isExcluded(): bool {
    return $this->excluded;
  }

  /**
   * {@inheritDoc}
   */
  public function setExcluded(bool $bool): void {
    $this->excluded = $bool;
  }

  /**
   * {@inheritDoc}
   */
  public function toArray(): array {
    $data = $this->data;
    $data[self::METADATA_KEY] = [
      'id' => $this->id,
      'uuid' => $this->getUuid,
      'type' => static::TYPE,
      'additional' => $this->getAdditionalMetadata(),
    ];

    return $data;
  }

  /**
   * @param array $array
   *
   * @return self
   */
  public static function fromArray(array $array): ?SnapshotItemInterface {

    $type = static::extractTypeFromArray($array);

    if ($type !== static::TYPE) {
      return NULL;
    }

    $metadataArray = $array[self::METADATA_KEY];
    unset($array[self::METADATA_KEY]);

    return new static($metadataArray['id'], $metadataArray['uuid'], $array);
  }

  protected function getAdditionalMetadata(): array {
    return [];
  }

  protected static function extractIdFromArray(array $array): string {
    return $array[self::METADATA_KEY]['id'];
  }

  protected static function extractTypeFromArray(array $array): string {
    return $array[self::METADATA_KEY]['type'];
  }

  protected static function extractUuidFromArray(array $array): bool {
    return (bool) $array[self::METADATA_KEY]['uuid'];
  }

  protected static function extractAdditionalMetadataFromArray(array $array): array {
    return $array[self::METADATA_KEY]['additional'];
  }

  protected static function removeMetadataFromArray($array): array {
    unset($array[self::METADATA_KEY]);

    return $array;
  }

}
