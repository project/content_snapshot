<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Snapshot;

/**
 * Class ContentEntitySnapshotItem.
 */
class ContentEntitySnapshotItem extends EntitySnapshotItem implements ContentEntitySnapshotItemInterface {
  /**
   * @var string
   */
  protected const TYPE = 'content_entity';

  /**
   * @var bool
   */
  private $isTranslatable;

  /**
   * EntitySnapshotItem constructor.
   *
   * @param string $id
   * @param string $uuid
   * @param array $data
   * @param string $entityType
   * @param string $bundle
   * @param bool $isTranslatable
   */
  public function __construct(
    string $id,
    string $uuid,
    array $data,
    string $entityType,
    string $bundle,
    bool $isTranslatable
  ) {

    parent::__construct($id, $uuid, $data, $entityType, $bundle);
    $this->isTranslatable = $isTranslatable;
  }

  /**
   * {@inheritDoc}
   */
  public function getNormalizedFields(): array {
    $content = $this->getNormalizedContent();
    unset($content[static::TRANSLATIONS_KEY]);

    return $content;
  }

  /**
   * {@inheritDoc}
   */
  public function getPath(): ?string {
    return 'content_entity' . DIRECTORY_SEPARATOR . $this->getEntityType() . DIRECTORY_SEPARATOR . $this->getBundle();
  }

  /**
   * {@inheritDoc}
   */
  public function getTranslation(string $langcode): ?array {
    $content = $this->getNormalizedContent();

    $translation = $content[static::TRANSLATIONS_KEY][$langcode] ?? NULL;
    //if ($translation) {
    //  $translation += $content;
    //}

    return $translation;
  }

  /**
   * {@inheritDoc}
   */
  public function isTranslatable(): bool {
    return $this->isTranslatable;
  }

  /**
   * {@inheritDoc}
   *
   * @return self
   */
  public static function fromArray(array $array): ?SnapshotItemInterface {

    $type = self::extractTypeFromArray($array);

    if ($type !== static::TYPE) {
      return NULL;
    }

    $additionalMetadata = self::extractAdditionalMetadataFromArray($array);

    $id = self::extractIdFromArray($array);
    $hasUuid = self::extractUuidFromArray($array);

    $array = self::removeMetadataFromArray($array);

    $entityType = $additionalMetadata['entity_type'];
    $bundle = $additionalMetadata['bundle'];
    $isTranslatable = $additionalMetadata['is_translatable'];

    return new static($id, $hasUuid, $array, $entityType, $bundle, $isTranslatable);
  }

  /**
   * {@inheritDoc}
   */
  protected function getAdditionalMetadata(): array {
    $metadata = parent::getAdditionalMetadata();
    $metadata['is_translatable'] = $this->isTranslatable();

    return $metadata;
  }

}
