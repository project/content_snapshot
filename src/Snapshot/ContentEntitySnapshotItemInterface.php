<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Snapshot;

/**
 * Interface ContentEntitySnapshotItemInterface.
 */
interface ContentEntitySnapshotItemInterface extends EntitySnapshotItemInterface, TranslatableSnapshotItemInterface {

}
