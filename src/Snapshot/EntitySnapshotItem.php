<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Snapshot;

/**
 * Class EntitySnapshotItem.
 */
class EntitySnapshotItem extends SnapshotItem implements EntitySnapshotItemInterface {

  /**
   * @var string
   */
  private $entityType;

  /**
   * @var string
   */
  private $bundle;

  /**
   * @var string
   */
  protected const TYPE = 'entity';

  /**
   * EntitySnapshotItem constructor.
   *
   * @param string $id
   * @param string $getUuid
   * @param array $data
   * @param string $entityType
   * @param string $bundle
   */
  public function __construct(string $id, string $getUuid, array $data, string $entityType, string $bundle) {

    $this->entityType = $entityType;
    $this->bundle = $bundle;

    parent::__construct($id, $getUuid, $data);
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityType(): string {
    return $this->entityType;
  }

  /**
   * {@inheritDoc}
   */
  public function getBundle(): string {
    return $this->bundle;
  }

  /**
   * {@inheritDoc}
   */
  public function getPath(): ?string {
    return 'entity' . DIRECTORY_SEPARATOR . $this->getEntityType() . DIRECTORY_SEPARATOR . $this->getBundle();
  }

  /**
   * {@inheritDoc}
   *
   * @return self
   */
  public static function fromArray(array $array): ?SnapshotItemInterface {

    $type = self::extractTypeFromArray($array);

    if ($type !== static::TYPE) {
      return NULL;
    }

    $additionalMetadata = self::extractAdditionalMetadataFromArray($array);

    $id = self::extractIdFromArray($array);
    $hasUuid = self::extractUuidFromArray($array);

    $array = self::removeMetadataFromArray($array);

    $entity_type = $additionalMetadata['entity_type'];
    $bundle = $additionalMetadata['bundle'];


    return new static($id, $hasUuid, $array, $entity_type, $bundle);
  }

  /**
   * {@inheritDoc}
   */
  protected function getAdditionalMetadata(): array {
    return [
      'entity_type' => $this->getEntityType(),
      'bundle' => $this->getBundle(),
    ];
  }

}
