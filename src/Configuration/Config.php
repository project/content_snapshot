<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Configuration;

use Drupal\Core\Config\ImmutableConfig;

/**
 * Class ConfigManager.
 *
 * This is our own wrapper for Drupal's config object. We need it to be able to
 * override config on runtime. Config Factory Overrides don't allow that,
 * because they happen before Drush commands run, and we need to alter config
 * based on command input. I wish it could be solved better.
 */
class Config implements ConfigInterface {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * @var string
   */
  private $snapshotPath;

  /**
   * ConfigManager constructor.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   */
  public function __construct(ImmutableConfig $config) {
    $this->config = $config;
  }

  /**
   * {@inheritDoc}
   */
  public function getSnapshotPath(): string {
    return !empty($this->snapshotPath) ? $this->snapshotPath : $this->config->get('snapshot_path');
  }

  /**
   * {@inheritDoc}
   */
  public function overrideSnapshotPath(?string $snapshotPath) {
    $this->snapshotPath = $snapshotPath;
  }

  /**
   * {@inheritDoc}
   */
  public function getStrict(): int {
    return $this->config->get('strict');
  }

  /**
   * {@inheritDoc}
   */
  public function getSnapshotStreams(): array {
    return $this->config->get('snapshot_streams');
  }

}
