<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Configuration;

/**
 * Interface ConfigInterface.
 */
interface ConfigInterface {

  /**
   * @return string
   */
  public function getSnapshotPath(): string;

  /**
   * @param string|null $snapshotPath
   */
  public function overrideSnapshotPath(?string $snapshotPath);

  /**
   * @return int
   */
  public function getStrict(): int;

  /**
   * @return array
   */
  public function getSnapshotStreams(): array;

}
