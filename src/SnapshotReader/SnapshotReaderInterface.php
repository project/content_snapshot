<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\SnapshotReader;

/**
 * Interface SnapshotReaderInterface.
 */
interface SnapshotReaderInterface {

  /**
   * @return array
   */
  public function read(): array;

}
