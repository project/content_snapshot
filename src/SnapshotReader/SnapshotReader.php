<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\SnapshotReader;

use Drupal\content_snapshot\Configuration\ConfigInterface;
use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\content_snapshot\SnapshotWriter\SnapshotWriter;
use Drupal\content_snapshot\StreamWrapper\ContentSnapshotStream;
use Drupal\Core\File\FileSystemInterface;

/**
 * Class SnapshotReader.
 */
class SnapshotReader implements SnapshotReaderInterface {

  /**
   * @var \Drupal\content_snapshot\Configuration\ConfigInterface
   */
  private $config;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * @var \Drupal\content_snapshot\Encoder\YamlEncoder
   */
  private $yamlEncoder;

  public function __construct(ConfigInterface $config, FileSystemInterface $fileSystem, YamlEncoder $yamlEncoder) {
    $this->config = $config;
    $this->fileSystem = $fileSystem;
    $this->yamlEncoder = $yamlEncoder;
  }

  /**
   * {@inheritDoc}
   */
  public function read(): array {
    $snapshotPath = $this->config->getSnapshotPath();

    $scheme = ContentSnapshotStream::STREAM_PATH;

    if (!is_dir($scheme) || !is_readable($scheme)) {
      throw new \LogicException("Target directory for snapshot '$snapshotPath' doesn't exist or is not readable.");
    }

    $snapshot = [];

    $dataArray = $this->fileSystem->scanDirectory($scheme . SnapshotWriter::DATA_DIR, '/.+/');

    foreach ($dataArray as $data) {
      $fileContent = file_get_contents($data->uri);
      $snapshot[$data->uri] = $this->yamlEncoder->decode($fileContent, $this->yamlEncoder::FORMAT);
    }

    return $snapshot;
  }

}
