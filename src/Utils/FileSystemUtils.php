<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Utils;

use DirectoryIterator;
use Drupal\Core\File\FileSystemInterface;

/**
 * Class FileSystemUtils.
 */
class FileSystemUtils implements FileSystemUtilsInterface {

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * FilesystemUtilities constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritDoc}
   */
  public function copyDirectory(string $source, string $destination, string $excludePattern = ''): void {
    /** @var \RecursiveDirectoryIterator $iterator */
    /** @var \SplFileInfo $item */
    foreach (
     $iterator = new \RecursiveIteratorIterator(
      new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
      \RecursiveIteratorIterator::SELF_FIRST
     ) as $item
    ) {
      $subpathname = $iterator->getSubPathName();
      $pathname = $iterator->getPathname();
      $relativePathname = str_replace($source, '', $pathname);

      // Exclude files
      if (!empty($excludePattern) && preg_match("/$excludePattern/", $relativePathname)) {
        continue;
      }

      if ($item->isDir()) {
        if (!mkdir($concurrentDirectory = $destination . DIRECTORY_SEPARATOR . $subpathname) && !is_dir($concurrentDirectory)) {
          throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
      } else {
        copy($item, $destination . DIRECTORY_SEPARATOR . $subpathname);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function purgeDirectory($uri): void {
    foreach (glob($this->fileSystem->realpath($uri) . '/*') as $file) {
      $this->fileSystem->deleteRecursive($file);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getSubdirectories($path): array {
    $directories = [];

    $iterator = new DirectoryIterator($path);
    foreach ($iterator as $fileinfo) {
        if ($fileinfo->isDir() && !$fileinfo->isDot()) {
          $directories[] = $fileinfo->getFilename();
        }
    }

    return $directories;
  }
}
