<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Utils;

/**
 * Interface FileSystemUtilsInterface.
 */
interface FileSystemUtilsInterface {

  /**
   * @param string $source
   * @param string $destination
   * @param string $excludePattern
   */
  public function copyDirectory(string $source, string $destination, string $excludePattern = ''): void;

  /**
   * @param $uri
   */
  public function purgeDirectory($uri): void;

  /**
   * @param $path
   *
   * @return array
   */
  public function getSubdirectories($path): array;
}
