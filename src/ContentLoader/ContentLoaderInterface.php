<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ContentLoader;

use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;

/**
 * Interface ContentLoaderInterface.
 */
interface ContentLoaderInterface {

  /**
   * Looks for existing content based on the provided snapshot.
   *
   * @param \Drupal\content_snapshot\Snapshot\SnapshotItemInterface $snapshotItem
   *
   * @return mixed|null
   */
  public function load(SnapshotItemInterface $snapshotItem);

}
