<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ContentLoader;

use Drupal\content_snapshot\Snapshot\ContentEntitySnapshotItemInterface;
use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class ContentEntityContentLoader.
 */
class ContentEntityContentLoader implements ContentLoaderInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * ContentEntityProvider constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }
  /**
   * {@inheritDoc}
   */
  public function load(SnapshotItemInterface $snapshotItem): ?ContentEntityInterface {
    if (!$snapshotItem instanceof ContentEntitySnapshotItemInterface) {
      return NULL;
    }

    $entityType = $snapshotItem->getEntityType();
    $storage = $this->entityTypeManager->getStorage($entityType);
    $id = $snapshotItem->getId();
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $storage->load($id);

    return $entity;
  }

}
