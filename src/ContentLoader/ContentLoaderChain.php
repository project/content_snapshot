<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ContentLoader;

use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;

/**
 * Class ContentLoaderChain.
 */
class ContentLoaderChain implements ContentLoaderInterface {

  /**
   * @var \Drupal\content_snapshot\ContentLoader\ContentLoaderInterface[]
   */
  private $contentReaders = [];

  /**
   * @param \Drupal\content_snapshot\ContentLoader\ContentLoaderInterface $contentReader
   */
  public function addContentLoader(ContentLoaderInterface $contentReader) {
    $this->contentReaders[] = $contentReader;
  }

  /**
   * {@inheritDoc}
   */
  public function load(SnapshotItemInterface $snapshotItem) {
    foreach ($this->contentReaders as $contentReader) {
      if ($object = $contentReader->load($snapshotItem)) {
        return $object;
      }
    }

    return NULL;
  }

}
