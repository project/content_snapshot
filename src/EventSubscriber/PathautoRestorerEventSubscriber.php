<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\EventSubscriber;

use Drupal\content_snapshot\ContentLoader\ContentLoaderInterface;
use Drupal\content_snapshot\Snapshot\ContentEntitySnapshotItemInterface;
use Drupal\content_snapshot\Event\SnapshotItemEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ContentSnapshotEventSubscriber.
 */
class PathautoRestorerEventSubscriber implements EventSubscriberInterface {


  /**
   * @var \Drupal\content_snapshot\ContentLoader\ContentLoaderInterface
   */
  private $contentLoader;

  public function __construct(ContentLoaderInterface $contentLoader) {
    $this->contentLoader = $contentLoader;
  }

  public static function getSubscribedEvents() {
    return [
      SnapshotItemEvent::IMPORT_POST_FINISHING => 'postImport',
    ];
  }

  public function postImport(SnapshotItemEvent $contentSnapshotEvent) {
    $snapshotItem = $contentSnapshotEvent->getSnapshotItem();

    if (!$snapshotItem instanceof ContentEntitySnapshotItemInterface) {
      return;
    }

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->contentLoader->load($snapshotItem);
    if (!$entity) {
      return;
    }

    $normalized = $snapshotItem->getNormalizedContent();

    if (empty($normalized['path'][0]['pathauto'])) {
      return;
    }

    if (!$entity->hasField('path') || $entity->getFieldDefinition('path')->getType() !== 'path') {
      return;
    }

    /** @var \Drupal\pathauto\PathautoFieldItemList $pathFieldList */
    $pathFieldList = $entity->get('path');

    /** @var \Drupal\pathauto\PathautoItem $pathFieldItem */
    $pathFieldItem = $pathFieldList->first();
    if (!$pathFieldItem) {
      return;
    }

    if (!array_key_exists('pathauto', $pathFieldItem->getProperties(TRUE))) {
      return;
    }

    $pathauto = $pathFieldItem->get('pathauto');

    if (!$pathauto instanceof \Drupal\pathauto\PathautoState) {
      return;
    }

    $pathauto->setValue((int) $normalized['path'][0]['pathauto']);
    $pathauto->persist();
  }

}
