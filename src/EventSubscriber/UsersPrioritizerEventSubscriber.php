<?php

/**
 * @file
 *
 * Files need to be imported first, because some other entities depends on
 * existance of files. For example imported "media" will generate new thumbnail
 * files if it won't find existing ones, and we will end up with duplicates.
 */

namespace Drupal\content_snapshot\EventSubscriber;

use Drupal\content_snapshot\Event\SnapshotItemEvent;
use Drupal\content_snapshot\Snapshot\ContentEntitySnapshotItemInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UsersPrioritizerEventSubscriber.
 *
 * Users have to be imported first, or node's "owner" reference may get broken
 * if node is imported before its owner.
 * See: https://www.drupal.org/project/drupal/issues/2376999
 */
class UsersPrioritizerEventSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    return [
      SnapshotItemEvent::IMPORT_POST_CONVERTING => 'preImport',
    ];
  }

  public function preImport(SnapshotItemEvent $contentSnapshotEvent) {
    $snapshotItem = $contentSnapshotEvent->getSnapshotItem();

    if (!$snapshotItem instanceof ContentEntitySnapshotItemInterface) {
      return;
    }

    if ($snapshotItem->getEntityType() !== 'user') {
      return;
    }

    $snapshotItem->setPriority(50);
  }

}
