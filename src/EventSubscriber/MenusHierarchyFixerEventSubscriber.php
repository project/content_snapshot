<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\EventSubscriber;

use Drupal\content_snapshot\Event\SnapshotEvent;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class MenusHierarchyFixerEventSubscriber.
 *
 * This subscriber fixes broken menu link hierarchy after import. Basically
 * what happens is that during the initial import, not all links are available.
 * If parent of the currently imported link is not available, information about
 * the hierarchy in menu_tree table for this link will be wrong. We can fix
 * this after all links are imported.
 */
class MenusHierarchyFixerEventSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  private $menuLinkContentStorage;

  /**
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  private $menuLinkManager;

  public function __construct(ContentEntityStorageInterface $menuLinkContentStorage, MenuLinkManagerInterface $menuLinkManager) {
    $this->menuLinkContentStorage = $menuLinkContentStorage;
    $this->menuLinkManager = $menuLinkManager;
  }

  public static function getSubscribedEvents() {
    return [
      SnapshotEvent::IMPORT_POST_INITIAL_IMPORT => 'postImport',
    ];
  }

  public function postImport(SnapshotEvent $contentSnapshotEvent) {
    $links = $this->menuLinkContentStorage->loadMultiple();

    /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $link */
    foreach ($links as $link) {
      $this->menuLinkManager->updateDefinition($link->getPluginId(), $link->getPluginDefinition(), FALSE);
    }
  }

}
