<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\EventSubscriber;

use Drupal\content_snapshot\Event\SnapshotItemEvent;
use Drupal\content_snapshot\Snapshot\ContentEntitySnapshotItemInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class FilesPrioritizerEventSubscriber.
 *
 * Files need to be imported early, because some other entities depends on
 * existance of files. For example imported "media" will generate new thumbnail
 * files if it won't find existing ones, and we will end up with duplicates.
 */
class FilesPrioritizerEventSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    return [
      SnapshotItemEvent::IMPORT_POST_CONVERTING => 'preImport',
    ];
  }

  public function preImport(SnapshotItemEvent $contentSnapshotEvent) {
    $snapshotItem = $contentSnapshotEvent->getSnapshotItem();

    if (!$snapshotItem instanceof ContentEntitySnapshotItemInterface) {
      return;
    }

    if ($snapshotItem->getEntityType() !== 'file') {
      return;
    }

    $data = $snapshotItem->getNormalizedContent();
    if (empty($data['uri'][0]['value'])) {
      return;
    }

    $snapshotItem->setPriority(10);
  }

}
