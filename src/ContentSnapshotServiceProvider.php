<?php

namespace Drupal\content_snapshot;

use Drupal\content_snapshot\Compiler\ContentSnapshotExtensionPass;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

class ContentSnapshotServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->addCompilerPass(new ContentSnapshotExtensionPass());
  }

}
