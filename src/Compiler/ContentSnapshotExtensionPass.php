<?php

namespace Drupal\content_snapshot\Compiler;

use Drupal\content_snapshot\EventSubscriber\MenusHierarchyFixerEventSubscriber;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class ContentSnapshotExtensionPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function process(ContainerBuilder $container) {

    if ($container->hasDefinition('pathauto.generator')) {
      $pathItemNormalizerDefinition = $container->getDefinition('content_snapshot.normalizer.field_item.path_item');
      $pathItemNormalizerDefinition->addMethodCall('setPathautoGenerator', [new Reference('pathauto.generator')]);
    }

    /** @var \Drupal\Core\Extension\ModuleHandler $moduleHandler */
    $moduleHandler = $container->get('module_handler');
    if ($moduleHandler->moduleExists('menu_link_content')) {

      if (!$container->hasDefinition('content_snapshot.core.storage.menu_link_content')) {
        $definition = new Definition(ContentEntityStorageInterface::class, ['menu_link_content']);
        $definition->setFactory([new Reference('entity_type.manager'), 'getStorage']);
        $definition->setPublic(TRUE);
        $container->setDefinition('content_snapshot.core.storage.menu_link_content', $definition);
      }

      if (!$container->hasDefinition('content_snapshot.event_subscriber.menus_hierarchy_fixer')) {
        $definition = new Definition(MenusHierarchyFixerEventSubscriber::class, [new Reference('content_snapshot.core.storage.menu_link_content'), new Reference('plugin.manager.menu.link')]);
        $definition->addTag('event_subscriber');
        $definition->setPublic(TRUE);
        $container->setDefinition('content_snapshot.event_subscriber.menus_hierarchy_fixer', $definition);
      }
    }
  }

}
