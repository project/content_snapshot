<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Normalizer;

use Drupal\content_snapshot\Configuration\ConfigInterface;
use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\content_snapshot\Logger\LoggerInterface;
use Drupal\content_snapshot\Snapshot\ContentEntitySnapshotItem;
use Drupal\content_snapshot\Snapshot\TranslatableSnapshotItemInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\TypedData\TypedDataInternalPropertiesHelper;
use Drupal\serialization\Normalizer\ContentEntityNormalizer as BaseContentEntityNormalizer;

/**
 * Class ContentEntityNormalizer
 */
class ContentEntityNormalizer extends BaseContentEntityNormalizer {

  use ContentSnapshotNormalizerContextTrait;

  protected $format = YamlEncoder::FORMAT;

  /**
   * @var \Drupal\content_snapshot\Logger\LoggerInterface
   */
  private $logger;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * @var \Drupal\content_snapshot\Configuration\ConfigInterface
   */
  private $config;

  /**
   * ContentEntityNormalizer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityTypeRepositoryInterface $entity_type_repository
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\content_snapshot\Logger\LoggerInterface $logger
   * @param \Drupal\content_snapshot\Configuration\ConfigInterface $config
   */
  public function __construct(
    EntityTypeManagerInterface    $entity_type_manager,
    EntityTypeRepositoryInterface $entity_type_repository,
    EntityFieldManagerInterface   $entity_field_manager,
    LoggerInterface               $logger,
    ConfigInterface               $config) {
    $this->logger = $logger;
    $this->config = $config;

    parent::__construct($entity_type_manager, $entity_type_repository, $entity_field_manager);
  }

  /**
   * {@inheritdoc}
   *
   * @param $entity ContentEntityInterface
   */
  public function normalize($entity, $format = NULL, array $context = []) {

    $this->validateContext($context);

    $attributes = [];
    /** @var \Drupal\Core\Entity\Entity $entity */
    foreach (TypedDataInternalPropertiesHelper::getNonInternalProperties($entity->getTypedData()) as $name => $field_items) {
      $attributes[$name] = $this->serializer->normalize($field_items, $format, $context);
    }

    // If "path" property is affected by base field override, it will get the
    // "internal" status for some reason. We need to get around this.
    if (!isset($attributes['path']) && $entity->hasField('path') && $entity->getFieldDefinition('path')->getType() === 'path') {
      $attributes['path'] = $this->serializer->normalize($entity->get('path'), $format, $context);
    }

    $this->cleanupNormalization($attributes, $entity);

    if ($entity->isTranslatable()) {

      $translatableFields = $entity->getTranslatableFields();
      $translatableFieldList = array_keys($translatableFields);

      $languages = $entity->getTranslationLanguages(FALSE);

      if (!empty($languages)) {
        $attributes[TranslatableSnapshotItemInterface::TRANSLATIONS_KEY] = [];
        $translationsAttributes = &$attributes[TranslatableSnapshotItemInterface::TRANSLATIONS_KEY];

        foreach ($languages as $langcode => $language) {

          $translation = $entity->getTranslation($langcode);
          $translationAttributes = [];

          foreach (TypedDataInternalPropertiesHelper::getNonInternalProperties($translation->getTypedData()) as $name => $fieldItems) {
            // If we are dealing with translation, then we want to return only translatable fields.
            if (!isset($translatableFieldList) || empty($translatableFieldList) || in_array($name, $translatableFieldList, TRUE)) {
              $translationAttributes[$name] = $this->serializer->normalize($fieldItems, $format, $context);
              unset($translationAttributes['langcode'], $translationAttributes['default_langcode']);
            }
          }

          // If "path" property is affected by base field override, it will get the
          // "internal" status for some reason. We need to get around this..
          if (!isset($translatableFieldList) || empty($translatableFieldList) || in_array('path', $translatableFieldList, TRUE)) {
            if (!isset($translationAttributes['path']) && $entity->hasField('path') && $entity->getFieldDefinition('path')->getType() === 'path') {
              $translationAttributes['path'] = $this->serializer->normalize($translation->get('path'), $format, $context);
            }
          }

          $this->cleanupNormalization($translationAttributes, $translation);
          $translationsAttributes[$langcode] = $translationAttributes;
        }
      }
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {

    $this->validateContext($context);

    $snapshotItem = ContentEntitySnapshotItem::fromArray($data);
    if (!$snapshotItem) {
      throw new \InvalidArgumentException("Wrong data has been provided to this denormalizer.");
    }

    $normalized = $snapshotItem->getNormalizedContent();


    if (!empty($normalized[TranslatableSnapshotItemInterface::TRANSLATIONS_KEY])) {
      $translations = $normalized[TranslatableSnapshotItemInterface::TRANSLATIONS_KEY];
      unset($normalized[TranslatableSnapshotItemInterface::TRANSLATIONS_KEY]);
    }

    /** @var EntityFieldManagerInterface $entityFieldManager */
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fieldDefinitions = $entityFieldManager->getFieldDefinitions($snapshotItem->getEntityType(), $snapshotItem->getBundle());

    foreach ($normalized as $fieldName => $field) {
      if (!array_key_exists($fieldName, $fieldDefinitions)) {
        $this->handleStrictLevel($snapshotItem->getEntityType(), $snapshotItem->getBundle(), $snapshotItem->getId(), $fieldName);
        unset($normalized[$fieldName]);
      }
    }

    /** @var ContentEntityInterface $entity */
    $entity = parent::denormalize($normalized, $class, $format, $context);

    if (isset($translations)) {
      foreach ($translations as $langcode => $translation) {
        $translatedEntity = $entity->addTranslation($langcode);

        // We need to denormalize our translation fields first.
        foreach ($translation as $fieldName => $translatedField) {

          if (!$translatedEntity->hasField($fieldName)) {
            $this->handleStrictLevel($snapshotItem->getEntityType(), $snapshotItem->getBundle(), $snapshotItem->getId(), $fieldName);
            continue;
          }

          $fieldItemList = $translatedEntity->get($fieldName);
          // Value may be automatically set on translation creation. We need to
          // make sure it's empty because new, denormalized values won't
          // replace it, but will be appended.
          $fieldItemList->setValue(NULL);
          $this->serializer->denormalize($translatedField, get_class($fieldItemList), $format, ['target_instance' => $fieldItemList] + $context);
        }

      }
    }

    return $entity;
  }

  private function handleStrictLevel(string $entityType, string $bundle, string $id, string $fieldName) {

    $message = "Denormalization: Snapshotted content entity (type: $entityType, bundle: $bundle, id: $id) has data for field '$fieldName' that has no definition in the database, so this data couldn't be imported.";

    $strictLevel = $this->config->getStrict();

    // If we are not strict, we can ignore additinal fields, that are not
    // represented in the database.
    switch ($strictLevel) {
      case 2:
        throw new \LogicException("($message You can disable this exception by disabling the 'strict' setting.");
      case 1:
        $this->logger->warning($message);
        break;
    }
  }

  /**
   * @param array $attributes
   * @param \Drupal\Core\Entity\ContentEntityInterface $contentEntity
   */
  private function cleanupNormalization(array &$attributes, ContentEntityInterface $contentEntity): void {

    // This value is meaningless as long as we are not snapshotting revisions.
    unset($attributes['revision_translation_affected']);

    if ($revisionKey = $contentEntity->getEntityType()->getKey('revision')) {
      unset($attributes[$revisionKey]);
    }
  }

}
