<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Normalizer;

use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\content_snapshot\Logger\LoggerInterface;
use Drupal\content_snapshot\ReferenceRestorer\ReferenceRestorerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\serialization\Normalizer\EntityReferenceFieldItemNormalizer as BaseEntityReferenceFieldItemNormalizer;

/**
 * Class EntityReferenceFieldItemNormalizer
 */
class EntityReferenceFieldItemNormalizer extends BaseEntityReferenceFieldItemNormalizer {

  use ContentSnapshotNormalizerContextTrait;

  protected $format = YamlEncoder::FORMAT;

  /**
   * @var \Drupal\content_snapshot\Logger\LoggerInterface
   */
  private $logger;


  /**
   * {@inheritDoc}
   */
  public function __construct(EntityRepositoryInterface $entity_repository, LoggerInterface $logger, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($entity_repository);
    $this->logger = $logger;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   *
   * @param $field \Drupal\Core\Field\FieldItemInterface
   */
  public function normalize($field, $format = NULL, array $context = []) {

    $this->validateContext($context);

    $attributes = parent::normalize($field, $format, $context);

    $this->cleanupNormalization($attributes);

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {

    $this->validateContext($context);

    /** @var \Drupal\Core\Field\FieldItemInterface $targetInstance */
    $targetInstance = $context['target_instance'];
    /** @var \Drupal\Core\Field\FieldItemListInterface $fieldListItem */
    $fieldListItem = $targetInstance->getParent();
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $fieldListItem->getParent()->getValue();

    $fieldType = $targetInstance->getFieldDefinition()->getType();
    $fieldName = $fieldListItem->getName();
    $entityTypeId = $entity->getEntityTypeId();
    $parentEntityId = $entity->id();

    if ($fieldType === 'entity_reference_revisions') {
      $paragraphStorage = $this->getEntityTypeManager()->getStorage($data['target_type']);
      $targetEntity = $paragraphStorage->load($data['target_id']);

      if (!$targetEntity) {
        if (!empty($context[ReferenceRestorerInterface::RESTORING_REFERENCES_CONTEXT])) {
          // If we cannot restore this reference even during the reference restoring phase, then inform the user.
          $this->logger->warning("Field '$fieldName' of the type '$fieldType' of an entity '$entityTypeId' with id '$parentEntityId' have broken revision reference so its value has been nulled.");
        }

        return NULL;
      }
      // Restore required property.
      $data['target_revision_id'] = $targetEntity->getRevisionId();
    }

    // Now that we have all entities in the database, we can denormalize all
    // entity reference fields normally.
    return parent::denormalize($data, $class, $format, $context);
  }


  /**
   * @param array $attributes
   */
  protected function cleanupNormalization(array &$attributes): void {
    // We don't want to store ids that are database dependent if we have uuid
    // available.

    // We don't need that, so let's reduce the size and readability of the
    // snapshot slightly.
    unset($attributes['url']);

    // This is not needed for restoring a reference, so we are removing it to
    // reduce redundant information in snapshot.
    unset($attributes['target_uuid']);

    // This is set and required by the entity_reference_revisions field,
    // but as we don't support the revisions yet, we will just find the latest
    // revision dynamically, during the denormalization.
    unset($attributes['target_revision_id']);
  }

}
