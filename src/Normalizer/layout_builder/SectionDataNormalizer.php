<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Normalizer\layout_builder;

use Drupal\block_content\BlockContentInterface;
use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\content_snapshot\Normalizer\ContentSnapshotNormalizerContextTrait;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\layout_builder\Plugin\DataType\SectionData;
use Drupal\layout_builder\Section;
use Drupal\serialization\Normalizer\TypedDataNormalizer;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Normalizes section data.
 */
class SectionDataNormalizer extends TypedDataNormalizer implements DenormalizerInterface {

  use ContentSnapshotNormalizerContextTrait;

  private const METADATA_KEY = '_content_snapshot';
  private const TARGET_UUID_KEY = 'target_uuid';

  protected $format = YamlEncoder::FORMAT;

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = SectionData::class;

  /** @var EntityRepositoryInterface */
  private $entityRepository;

  /** @var EntityTypeManagerInterface */
  private $entityTypeManager;

  public function __construct(EntityRepositoryInterface $entityRepository, EntityTypeManagerInterface $entityTypeManager) {
    $this->entityRepository = $entityRepository;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {

    $this->validateContext($context);

    $array = $object->getValue()->toArray();

    $components = &$array['components'];

    foreach ($components as $key => &$component) {
      $config = &$component['configuration'];

      if (strpos($config['id'], 'inline_block') === 0) {
        $storage = $this->entityTypeManager->getStorage('block_content');
        if (empty($config[static::METADATA_KEY])) {
          $block_revision_id = $config['block_revision_id'];
          $block = $storage->loadRevision($block_revision_id);
          if (null === $block) {
            // If referenced block doesn't exist, remove it from sections.
            unset($components[$key]);
            continue;
          }
          $config[static::METADATA_KEY] = [
            static::TARGET_UUID_KEY => $block->uuid(),
          ];
          unset($config['block_revision_id']);
          continue;
        }

        /** @var BlockContentInterface $blockContent */
        $blockContent = $this->entityRepository->loadEntityByUuid('block_content', $config[static::METADATA_KEY][static::TARGET_UUID_KEY]);

        if (!$blockContent) {
          // If referenced block doesn't exist, do not export its reference.
          unset($components[$key]);
        }
      }

    }

    return $array;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {

    $this->validateContext($context);

    if (!isset($context['target_instance'])) {
      throw new InvalidArgumentException('$context[\'target_instance\'] must be set to denormalize with the SectionDataNormalizer');
    }

    /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
    $target_instance = $context['target_instance'];

    $data = Section::fromArray($data);

    $components = $data->getComponents();
    /** @var \Drupal\layout_builder\SectionComponent $component */
    foreach ($components as $component) {
      $config = $component->toArray()['configuration'];

      if (strpos($config['id'], 'inline_block') === 0) {

        if (empty($config[static::METADATA_KEY])) {
          continue;
        }

        /** @var BlockContentInterface|null $blockContent */
        $blockContent = $this->entityRepository->loadEntityByUuid('block_content', $config[static::METADATA_KEY][static::TARGET_UUID_KEY]);

        unset($config[static::METADATA_KEY]);

        // Do not import configuration that has a reference to a non-existent
        // block.
        if (!$blockContent) {
          continue;
        }

        $config['block_revision_id'] = $blockContent->getRevisionId();
        $component->setConfiguration($config);
      }
    }

    $object = SectionData::createInstance($target_instance->getDataDefinition());
    $object->setValue($data);

    return $object;
  }
}
