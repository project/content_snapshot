<?php

/**
 * @file
 *
 * We need this normalizer to correctly denormalize layout builder's sections.
 */

namespace Drupal\content_snapshot\Normalizer\layout_builder;

use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\content_snapshot\Normalizer\ContentSnapshotNormalizerContextTrait;
use Drupal\layout_builder\Plugin\DataType\SectionData;
use Drupal\layout_builder\Plugin\Field\FieldType\LayoutSectionItem;
use Drupal\serialization\Normalizer\FieldItemNormalizer;

/**
 * Normalizes layout builder's section.
 *
 * @internal
 */
class LayoutSectionItemNormalizer extends FieldItemNormalizer {

  use ContentSnapshotNormalizerContextTrait;

  protected $format = YamlEncoder::FORMAT;

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = LayoutSectionItem::class;

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {

    $this->validateContext($context);

    /** @var \Drupal\layout_builder\Plugin\DataType\SectionData $section_data */
    $section_data = $this->serializer->denormalize($data['section'], SectionData::class, $format, $context);
    /** @var LayoutSectionItem $target_instance */
    $target_instance = $context['target_instance'];
    $target_instance->setValue(['section' => $section_data->getValue()]);

    return $target_instance;
  }

}
