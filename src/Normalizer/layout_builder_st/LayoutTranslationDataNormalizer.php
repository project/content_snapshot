<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Normalizer\layout_builder_st;

use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\content_snapshot\Normalizer\ContentSnapshotNormalizerContextTrait;
use Drupal\layout_builder_st\Plugin\DataType\LayoutTranslationData;
use Drupal\serialization\Normalizer\TypedDataNormalizer;

/**
 * Class LayoutTranslationDataNormalizer.
 */
class LayoutTranslationDataNormalizer extends TypedDataNormalizer {

  use ContentSnapshotNormalizerContextTrait;

  protected $format = YamlEncoder::FORMAT;

  protected $supportedInterfaceOrClass = LayoutTranslationData::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {

    $this->validateContext($context);

    $normalized = parent::normalize($object, $format, $context);

    if (!empty($normalized['components'])) {
      foreach ($normalized['components'] as &$component) {
        // We cannot leave it here, because it can cause broken references on
        // translated pages after the import.
        unset($component['block_revision_id']);
      }
    }

    return $normalized;
  }

}
