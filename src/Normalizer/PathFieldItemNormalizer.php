<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Normalizer;

use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\path\Plugin\Field\FieldType\PathItem;
use Drupal\pathauto\PathautoGeneratorInterface;

/**
 * Class PathFieldItemNormalizer.
 */
class PathFieldItemNormalizer extends \Drupal\serialization\Normalizer\FieldItemNormalizer {

  use ContentSnapshotNormalizerContextTrait;

  protected $format = YamlEncoder::FORMAT;

  protected $supportedInterfaceOrClass = PathItem::class;

  /** @var \Drupal\pathauto\PathautoGeneratorInterface|null */
  private $pathautoGenerator;

  /**
   * PathItemNormalizer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @param \Drupal\pathauto\PathautoGeneratorInterface $pathautoGenerator
   */
  public function setPathautoGenerator(PathautoGeneratorInterface $pathautoGenerator) {
    $this->pathautoGenerator = $pathautoGenerator;
  }

  /**
   * {@inheritdoc}
   *
   * @param $field \Drupal\Core\Field\FieldItemInterface
   */
  public function normalize($field, $format = NULL, array $context = []) {

    $this->validateContext($context);
    $attributes = parent::normalize($field, $format, $context);
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $field->getParent()->getParent()->getValue();

    $fixedAttributes = [];
    if ($this->pathautoGenerator && array_key_exists('pathauto', $field->getProperties(TRUE))) {
      $pathauto = $field->get('pathauto');
      if (
        get_class($pathauto) === 'Drupal\pathauto\PathautoState' &&
        $this->pathautoGenerator->getPatternByEntity($entity)
      ) {
        $fixedAttributes['pathauto'] = $pathauto->getValue();
      }
    }

    if (!$attributes['pid']) {
      return NULL;
    }

    unset($attributes['pid']);

    return $fixedAttributes + $attributes;
  }

  /**
   * {@inheritDoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {

    $this->validateContext($context);

    /** @var \Drupal\Core\Field\FieldItemInterface $targetInstance */
    $targetInstance = $context['target_instance'];

    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $targetInstance->getParent()->getParent()->getValue();

    // We need to make sure that pathauto is disabled during the whole
    // importing or we are risking creating duplicated path aliases.
    if ($this->pathautoGenerator && array_key_exists('pathauto', $targetInstance->getProperties(TRUE))) {
      /** @var \Drupal\pathauto\PathautoState $pathauto */
      $pathauto = $targetInstance->get('pathauto');

      if ($pathauto instanceof \Drupal\pathauto\PathautoState) {
        if (
          isset($data['pathauto']) &&
          $this->pathautoGenerator->getPatternByEntity($entity)
        ) {
          $pathauto->setValue(0, FALSE);
        }
      }
    }

    return NULL;
  }

}
