<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Normalizer;

use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\PasswordItem;
use Drupal\pathauto\PathautoGeneratorInterface;

/**
 * Class PasswordFieldItemNormalizer.
 */
class PasswordFieldItemNormalizer extends \Drupal\serialization\Normalizer\FieldItemNormalizer {

  use ContentSnapshotNormalizerContextTrait;

  protected $format = YamlEncoder::FORMAT;

  protected $supportedInterfaceOrClass = PasswordItem::class;

  /** @var \Drupal\pathauto\PathautoGeneratorInterface|null */
  private $pathautoGenerator;

  /**
   * PathItemNormalizer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @param \Drupal\pathauto\PathautoGeneratorInterface $pathautoGenerator
   */
  public function setPathautoGenerator(PathautoGeneratorInterface $pathautoGenerator) {
    $this->pathautoGenerator = $pathautoGenerator;
  }

  /**
   * {@inheritdoc}
   *
   * @param $field \Drupal\Core\Field\FieldItemInterface
   */
  public function normalize($field, $format = NULL, array $context = []) {

    $this->validateContext($context);

    $attributes = parent::normalize($field, $format, $context);
    $attributes['pre_hashed'] = TRUE;
    unset($attributes['existing']);

    return $attributes;
  }

  /**
   * {@inheritDoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {

    $this->validateContext($context);

    return parent::denormalize($data, $class, $format, $context);
  }

}
