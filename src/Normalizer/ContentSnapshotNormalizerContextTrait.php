<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Normalizer;

/**
 * Trait ContentSnapshotNormalizerContextTrait
 *
 * @package Drupal\content_snapshot\Normalizer
 */
trait ContentSnapshotNormalizerContextTrait {

  public static $CONTENT_SNAPSHOT_CONTEXT = 'content_snapshot_context';

  /**
   * @param $context
   */
  private function validateContext(&$context) {
    $context += [
      self::$CONTENT_SNAPSHOT_CONTEXT => FALSE,
    ];

    if (!$context[self::$CONTENT_SNAPSHOT_CONTEXT]) {
      throw new \LogicException("This normalizer is supposed to be used only in the Content Snapshot context. Check your normalizers priorities.");
    }
  }

}
