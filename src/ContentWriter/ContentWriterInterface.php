<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ContentWriter;

use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;

/**
 * Interface ContentWriterInterface.
 */
interface ContentWriterInterface {

  /**
   * @param $object
   *
   * @return bool
   */
  public function write($object): bool;

}
