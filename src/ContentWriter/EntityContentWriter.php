<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ContentWriter;

use Drupal\content_snapshot\Plugin\Field\FieldType\FreezableChangedItem;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\SynchronizableInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\TypedData\TypedDataInternalPropertiesHelper;
use Drupal\media\MediaInterface;

/**
 * Class EntityContentWriter.
 */
class EntityContentWriter implements ContentWriterInterface {

  /**
   * {@inheritDoc}
   */
  public function write($object): bool {

    if (!$object instanceof EntityInterface) {
      return FALSE;
    }

    if ($object instanceof SynchronizableInterface) {
      $object->setSyncing(TRUE);
    }

    $this->updateFields($object);

    if ($object instanceof TranslatableInterface && $object->isTranslatable()) {
      $languages = $object->getTranslationLanguages();
      foreach ($languages as $langcode => $language) {
        $translation = $object->getTranslation($langcode);
        $this->updateFields($translation);
      }
    }

    if ($object instanceof RevisionLogInterface ) {
      // We don't want for revision creation time to be changed with each
      // import.
      if ($object instanceof FieldableEntityInterface) {
        if ($lastDate = $this->getLastDate($object)) {
          $object->setRevisionCreationTime($lastDate);
        }
      }
    }

    $object->save();

    // We need to fix wrong Media's revision creation time, that cannot be set
    // during the first save, because of Media's special handling of this
    // property.
    // See: https://www.drupal.org/project/drupal/issues/2897026
    // TODO Remove this when the above issue is fixed.
    if ($object instanceof MediaInterface) {
      $object->setNewRevision(FALSE);
      $object->setRevisionCreationTime($lastDate);
      $object->save();
    }

    return TRUE;
  }

  private function updateFields(EntityInterface $entity) {
    foreach (TypedDataInternalPropertiesHelper::getNonInternalProperties($entity->getTypedData()) as $fieldName => $fieldItemsList) {
      $fieldDefinition = $fieldItemsList->getFieldDefinition();
      // Make sure that changed item won't be updated.
      if ($fieldDefinition->getItemDefinition()->getClass() === FreezableChangedItem::class) {
        foreach ($fieldItemsList as $freezableChangedItem) {
          $freezableChangedItem->content_snapshot_freeze = TRUE;
        }
      }
    }
  }

  private function getLastDate($object): ?int {
    $lastDate = NULL;

    if ($object->hasField('changed') && $object->get('changed')->getFieldDefinition()->getType() === 'changed') {
      $lastDate = (int) $object->get('changed')->first()->getValue()['value'];
    } elseif ($object->hasField('created') && $object->get('created')->getFieldDefinition()->getType() === 'created') {
      $lastDate = (int) $object->get('created')->first()->getValue()['value'];
    }

    return $lastDate;
  }

}
