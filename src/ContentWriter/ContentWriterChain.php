<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ContentWriter;

use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;

/**
 * Class ContentWriterChain.
 */
class ContentWriterChain implements ContentWriterInterface {

  /**
   * @var \Drupal\content_snapshot\ContentWriter\ContentWriterInterface[]
   */
  private $writers = [];

  /**
   * @param \Drupal\content_snapshot\ContentWriter\ContentWriterInterface $writer
   */
  public function addWriter(ContentWriterInterface $writer) {
    $this->writers[] = $writer;
  }

  /**
   * {@inheritDoc}
   */
  public function write($object): bool {
    foreach ($this->writers as $writer) {
      if ($writer->write($object)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
