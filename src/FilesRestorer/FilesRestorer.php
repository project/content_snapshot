<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\FilesRestorer;

use Drupal\content_snapshot\StreamWrapper\ContentSnapshotStream;
use Drupal\content_snapshot\Utils\FileSystemUtilsInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Class FilesRestorer.
 */
class FilesRestorer implements FilesRestorerInterface {

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * @var \Drupal\content_snapshot\Utils\FileSystemUtilsInterface
   */
  private $fileSystemUtils;

  /**
   * FilesRestorer constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \Drupal\content_snapshot\Utils\FileSystemUtilsInterface $fileSystemUtils
   */
  public function __construct(FileSystemInterface $fileSystem, FileSystemUtilsInterface $fileSystemUtils) {
    $this->fileSystem = $fileSystem;
    $this->fileSystemUtils = $fileSystemUtils;
  }

  /**
   * {@inheritDoc}
   */
  public function restoreStream(string $scheme): void {
    if (!$this->fileSystem->realpath("$scheme://")) {
      throw new \RuntimeException("Couldn't restore files from the '$scheme' stream. Stream is inaccessible");
    }
    $this->fileSystemUtils->purgeDirectory("$scheme://");
    $this->fileSystemUtils->copyDirectory(ContentSnapshotStream::STREAM_PATH . "files/$scheme", "$scheme://");
  }

}
