<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\FilesRestorer;

/**
 * Interface FilesRestorerInterface.
 */
interface FilesRestorerInterface {

  /**
   * @param string $scheme
   */
  public function restoreStream(string $scheme): void;
}
