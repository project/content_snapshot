<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Commands;

use Drupal\content_snapshot\Configuration\ConfigInterface;
use Drupal\content_snapshot\Exporter\ExporterInterface;
use Drupal\content_snapshot\Importer\ImporterInterface;
use Drupal\content_snapshot\Logger\LoggerInterface;
use Drupal\content_snapshot\Purger\PurgerInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush ^9 commands.
 */
class Commands extends DrushCommands {

  /**
   * @var \Drupal\content_snapshot\Exporter\ExporterInterface
   */
  private $exporter;

  /**
   * @var \Drupal\content_snapshot\Importer\ImporterInterface
   */
  private $importer;

  /**
   * @var \Drupal\content_snapshot\Logger\LoggerInterface
   */
  private $snapshotLogger;

  /**
   * @var \Drupal\content_snapshot\Purger\PurgerInterface
   */
  private $purger;

  /**
   * @var \Drupal\content_snapshot\Configuration\ConfigInterface
   */
  private $configManager;

  /**
   * @param \Drupal\content_snapshot\Exporter\ExporterInterface $exporter
   * @param \Drupal\content_snapshot\Importer\ImporterInterface $importer
   * @param \Drupal\content_snapshot\Purger\PurgerInterface $purger
   * @param \Drupal\content_snapshot\Logger\LoggerInterface $snapshotLogger
   * @param \Drupal\content_snapshot\Configuration\ConfigInterface $configManager
   */
  public function __construct(ExporterInterface $exporter, ImporterInterface $importer, PurgerInterface $purger, LoggerInterface $snapshotLogger, ConfigInterface $configManager) {
    parent::__construct();
    $this->exporter = $exporter;
    $this->importer = $importer;
    $this->snapshotLogger = $snapshotLogger;
    $this->purger = $purger;
    $this->configManager = $configManager;
  }

  /**
   * @command content-snapshot:export
   *
   * @option $snapshot-path Allows to override the snapshot path.
   *
   * @usage content-snapshot:export
   *
   * @validate-module-enabled content_snapshot
   */
  public function export(array $options = ['snapshot-path' => NULL]): void {

    if (!$this->io()->confirm('Are you sure you want to delete existing snapshot and replace it with newly generated one?', FALSE)) {
      return;
    }

    if ($options['snapshot-path']) {
      $this->configManager->overrideSnapshotPath($options['snapshot-path']);
    }

    $this->prepare();
    $this->exporter->export();
  }

  /**
   * @command content-snapshot:import
   *
   * @option $snapshot-path Allows to override the snapshot path.
   *
   * @usage content-snapshot:import
   *
   * @validate-module-enabled content_snapshot
   */
  public function import(array $options = ['snapshot-path' => NULL]): void {

    if (!$this->io()->confirm('Are you sure you want to destroy all existing content and import the last snapshot?', FALSE)) {
      return;
    }

    if ($options['snapshot-path']) {
      $this->configManager->overrideSnapshotPath($options['snapshot-path']);
    }

    $this->prepare();
    $this->importer->import();

    $this->snapshotLogger->info("Flushing all caches.");
    drupal_flush_all_caches();
  }

  /**
   * @command content-snapshot:purge
   *
   * @usage content-snapshot:purge
   *
   * @validate-module-enabled content_snapshot
   */
  public function purge(): void {

    if (!$this->io()->confirm('Are you sure you want to destroy all existing content?', FALSE)) {
      return;
    }

    $this->prepare();
    $this->purger->purge();
  }

  private function prepare(): void {
    // We are wrapping Drush's logger, exposing it to our services.
    $this->snapshotLogger->setLogger($this->logger());
  }

}
