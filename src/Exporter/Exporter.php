<?php

namespace Drupal\content_snapshot\Exporter;

use Drupal\content_snapshot\Configuration\ConfigInterface;
use Drupal\content_snapshot\Converter\ToSnapshotItemConverterInterface;
use Drupal\content_snapshot\Event\SnapshotEvent;
use Drupal\content_snapshot\Event\SnapshotItemEvent;
use Drupal\content_snapshot\Logger\LoggerInterface;
use Drupal\content_snapshot\ContentProvider\ContentProviderInterface;
use Drupal\content_snapshot\SnapshotWriter\SnapshotWriterInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class Exporter.
 */
class Exporter implements ExporterInterface {

  /**
   * @var \Drupal\content_snapshot\Converter\ToSnapshotItemConverterInterface
   */
  private $toSnapshotItemConverter;

  /**
   * @var ContentProviderInterface
   */
  private $contentProvider;

  /**
   * @var \Drupal\content_snapshot\SnapshotWriter\SnapshotWriterInterface
   */
  private $snapshotWriter;

  /**
   * @var \Drupal\content_snapshot\Logger\LoggerInterface
   */
  private $logger;

  /**
   * @var \Drupal\content_snapshot\Configuration\ConfigInterface
   */
  private $config;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * Exporter constructor.
   *
   * @param \Drupal\content_snapshot\Converter\ToSnapshotItemConverterInterface $toSnapshotItemConverter
   * @param \Drupal\content_snapshot\ContentProvider\ContentProviderInterface $contentProvider
   * @param \Drupal\content_snapshot\SnapshotWriter\SnapshotWriterInterface $snapshotWriter
   * @param \Drupal\content_snapshot\Logger\LoggerInterface $logger
   * @param \Drupal\content_snapshot\Configuration\ConfigInterface $config
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   */
  public function __construct(
    ToSnapshotItemConverterInterface $toSnapshotItemConverter,
    ContentProviderInterface         $contentProvider,
    SnapshotWriterInterface          $snapshotWriter,
    LoggerInterface                  $logger,
    ConfigInterface                  $config,
    EventDispatcherInterface         $eventDispatcher
  ) {
    $this->toSnapshotItemConverter = $toSnapshotItemConverter;
    $this->contentProvider = $contentProvider;
    $this->snapshotWriter = $snapshotWriter;
    $this->logger = $logger;
    $this->config = $config;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritDoc}
   */
  public function export(): void {

    $this->logger->info("Gathering content.");
    $content = $this->contentProvider->provide();

    $this->logger->info("Purging previous snapshot.");
    $this->snapshotWriter->purge();

    $this->logger->info("Started snapshotting process.");

    $snapshotItems = [];
    // Convert objects to snapshot items.
    foreach ($content as $object) {
      $snapshotItem = $this->toSnapshotItemConverter->objectToSnapshot($object);

      if (!$snapshotItem) {
        throw new \LogicException("Content item couldn't be converted to snapshot.");
      }

      $this->eventDispatcher->dispatch(SnapshotItemEvent::EXPORT_POST_CONVERTING, new SnapshotItemEvent($snapshotItem));

      if ($snapshotItem->isExcluded()) {
        continue;
      }

      $snapshotItems[] = $snapshotItem;
    }

    $this->eventDispatcher->dispatch(SnapshotEvent::EXPORT_POST_CONVERTING, new SnapshotEvent($snapshotItems));

    // Save snapshot items.
    foreach ($snapshotItems as $snapshotItem) {
      $id = $snapshotItem->getId();
      $path = $snapshotItem->getPath();
      $this->snapshotWriter->writeContentItem($id, $snapshotItem->toArray(), $path);

      $this->eventDispatcher->dispatch(SnapshotItemEvent::EXPORT_POST_SNAPSHOT_SAVING, new SnapshotItemEvent($snapshotItem));
    }

    $this->eventDispatcher->dispatch(SnapshotEvent::EXPORT_POST_SNAPSHOT_SAVING, new SnapshotEvent($snapshotItems));

    // Save streams
    $streams = $this->config->getSnapshotStreams();
    foreach ($streams as $stream) {
      $scheme = $stream['scheme'];
      $pathExcludePattern = $stream['path_exclude_pattern'];
      $this->snapshotWriter->writeStream($scheme, $pathExcludePattern);
    }

    $this->logger->info("Export is complete.");
  }

}
