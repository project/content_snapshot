<?php

namespace Drupal\content_snapshot\Exporter;

/**
 * Interface ExporterInterface.
 */
interface ExporterInterface {

  public const PROCESSING_PHASE_CONVERTING = 'converting';
  public const PROCESSING_PHASE_SNAPSHOT_SAVING = 'snapshot_saving';

  /**
   * @return void
   */
  public function export(): void;

}
