<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Event;

use Drupal\content_snapshot\Exporter\ExporterInterface;
use Drupal\content_snapshot\Importer\ImporterInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class SnapshotEvent
 */
class SnapshotEvent extends Event {

  private const EVENT_NAME_PREFIX = 'content_snapshot.snapshot';
  private const IMPORT_PREFIX = self::EVENT_NAME_PREFIX . '.import';
  private const EXPORT_PREFIX = self::EVENT_NAME_PREFIX . '.export';

  public const IMPORT_POST_CONVERTING = self::IMPORT_PREFIX . '.post_' . ImporterInterface::PROCESSING_PHASE_CONVERTING;
  public const IMPORT_POST_INITIAL_IMPORT = self::IMPORT_PREFIX . '.post_' . ImporterInterface::PROCESSING_PHASE_INITIAL_IMPORT;
  public const IMPORT_POST_REFERENCE_RESTORING = self::IMPORT_PREFIX . '.post_' . ImporterInterface::PROCESSING_PHASE_RESTORING_REFERENCES;
  public const IMPORT_POST_FINISHING = self::IMPORT_PREFIX . '.post_' . ImporterInterface::PROCESSING_PHASE_FINISHING;

  public const EXPORT_POST_CONVERTING = self::EXPORT_PREFIX . '.post_' . ExporterInterface::PROCESSING_PHASE_CONVERTING;
  public const EXPORT_POST_SNAPSHOT_SAVING = self::EXPORT_PREFIX . '.post_' . ExporterInterface::PROCESSING_PHASE_SNAPSHOT_SAVING;

  /**
   * @var \Drupal\content_snapshot\Snapshot\SnapshotItemInterface[]
   */
  private $snapshotItems;

  /**
   * SnapshotEvent constructor.
   *
   * @param array $snapshotItems
   */
  public function __construct(array &$snapshotItems) {
    $this->snapshotItems = &$snapshotItems;
  }

  /**
   * @return \Drupal\content_snapshot\Snapshot\SnapshotItemInterface[]
   */
  public function getSnapshotItems(): array {
    return $this->snapshotItems;
  }

}
