<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Event;

use Drupal\content_snapshot\Exporter\ExporterInterface;
use Drupal\content_snapshot\Importer\ImporterInterface;
use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ContentSnapshotItemEvent
 */
class SnapshotItemEvent extends Event {

  private const EVENT_NAME_PREFIX = 'content_snapshot.snapshot_item';
  private const IMPORT_PREFIX = self::EVENT_NAME_PREFIX . '.import';
  private const EXPORT_PREFIX = self::EVENT_NAME_PREFIX . '.export';

  public const IMPORT_POST_CONVERTING = self::IMPORT_PREFIX . '.post_' . ImporterInterface::PROCESSING_PHASE_CONVERTING;
  public const IMPORT_POST_INITIAL_IMPORT = self::IMPORT_PREFIX . '.post_' . ImporterInterface::PROCESSING_PHASE_INITIAL_IMPORT;
  public const IMPORT_POST_REFERENCE_RESTORING = self::IMPORT_PREFIX . '.post_' . ImporterInterface::PROCESSING_PHASE_RESTORING_REFERENCES;
  public const IMPORT_POST_FINISHING = self::IMPORT_PREFIX . '.post_' . ImporterInterface::PROCESSING_PHASE_FINISHING;

  public const EXPORT_POST_CONVERTING = self::EXPORT_PREFIX . '.post_' . ExporterInterface::PROCESSING_PHASE_CONVERTING;
  public const EXPORT_POST_SNAPSHOT_SAVING = self::EXPORT_PREFIX . '.post_' . ExporterInterface::PROCESSING_PHASE_SNAPSHOT_SAVING;

  /**
   * @var \Drupal\content_snapshot\Snapshot\SnapshotItemInterface
   */
  private $snapshotItem;

  /**
   * SnapshotItemEvent constructor.
   *
   * @param \Drupal\content_snapshot\Snapshot\SnapshotItemInterface $snapshotItem
   */
  public function __construct(SnapshotItemInterface $snapshotItem) {
    $this->snapshotItem = $snapshotItem;
  }

  /**
   * @return \Drupal\content_snapshot\Snapshot\SnapshotItemInterface
   */
  public function getSnapshotItem(): SnapshotItemInterface {
    return $this->snapshotItem;
  }

}
