<?php

namespace Drupal\content_snapshot\StreamWrapper;

use Drupal\Core\StreamWrapper\LocalStream;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\Url;

/**
 * Defines a (content-snapshot://) stream wrapper class.
 */
class ContentSnapshotStream extends LocalStream {

  public const STREAM_PATH = 'content-snapshot://';

  /**
   * {@inheritdoc}
   */
  public static function getType(): int {
    return StreamWrapperInterface::LOCAL_HIDDEN;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return t('Content snapshot files');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return t('Content snapshot files.');
  }

  /**
   * {@inheritdoc}
   */
  public function getDirectoryPath(): string {
    /** @var \Drupal\content_snapshot\Configuration\ConfigInterface $config */
    $config = \Drupal::service('content_snapshot.config');
    return  $config->getSnapshotPath();
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl(): void {
    throw new \LogicException('Content snapshot files URL should not be public.');
  }

}
