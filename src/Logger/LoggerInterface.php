<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Logger;

/**
 * Interface LoggerInterface.
 */
interface LoggerInterface extends \Psr\Log\LoggerInterface {

  /**
   * @param \Psr\Log\LoggerInterface $logger
   *
   * @return void
   */
  public function setLogger(\Psr\Log\LoggerInterface $logger): void;

}
