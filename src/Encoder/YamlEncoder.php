<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\Encoder;

use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Parser;

/**
 * We are using custom yaml encoder and format as long as Drupal doesn't have
 * one in core, after that happen, this class and format will become deprecated.
 */
class YamlEncoder implements EncoderInterface, DecoderInterface {

  /**
   * The formats that this Encoder supports.
   *
   * @var string
   */
  public const FORMAT = 'yaml_content_snapshot';

  /**
   * @var Dumper
   */
  private $dumper;

  /**
   * @var Parser
   */
  private $parser;

  public function __construct() {
    $this->dumper = new Dumper(2);
    $this->parser = new Parser();
  }


  public function encode($data, $format, array $context = []) {
    return $this->dumper->dump($data, PHP_INT_MAX);
  }

  public function supportsEncoding($format) {
    return $format === static::FORMAT;
  }

  public function decode($data, $format, array $context = []) {
    return $this->parser->parse($data);
  }

  public function supportsDecoding($format) {
    return $format === static::FORMAT;
  }

}
