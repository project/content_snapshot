<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ReferenceRestorer;

use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;

/**
 * Class ReferenceResolverChain.
 */
class ReferenceRestorerChain implements ReferenceRestorerInterface {

  /**
   * @var \Drupal\content_snapshot\ReferenceRestorer\ReferenceRestorerInterface[]
   */
  private $referenceRestorers = [];

  /**
   * @param \Drupal\content_snapshot\ReferenceRestorer\ReferenceRestorerInterface $referenceRestorer
   */
  public function addReferenceRestorer(ReferenceRestorerInterface $referenceRestorer) {
    $this->referenceRestorers[] = $referenceRestorer;
  }

  /**
   * {@inheritDoc}
   */
  public function restoreReferences(SnapshotItemInterface $snapshotItem, &$object): bool {
    foreach ($this->referenceRestorers as $resolver) {
      if ($result = $resolver->restoreReferences($snapshotItem, $object)) {
        return $result;
      }
    }

    return FALSE;
  }

}
