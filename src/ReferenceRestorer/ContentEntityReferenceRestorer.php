<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ReferenceRestorer;

use Drupal\content_snapshot\Encoder\YamlEncoder;
use Drupal\content_snapshot\Importer\ImporterInterface;
use Drupal\content_snapshot\Normalizer\ContentSnapshotNormalizerContextTrait;
use Drupal\content_snapshot\Snapshot\ContentEntitySnapshotItemInterface;
use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Class ContentEntityReferenceRestorer.
 */
class ContentEntityReferenceRestorer implements ReferenceRestorerInterface {

  /**
   * @var \Symfony\Component\Serializer\Normalizer\DenormalizerInterface
   */
  private $denormalizer;

  /**
   * ContentEntityReferenceRestorer constructor.
   *
   * @param \Symfony\Component\Serializer\Normalizer\DenormalizerInterface $denormalizer
   */
  public function __construct(DenormalizerInterface $denormalizer) {
    $this->denormalizer = $denormalizer;
  }

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   */
  public function restoreReferences(SnapshotItemInterface $snapshot, &$entity): bool {
    if (!$snapshot instanceof ContentEntitySnapshotItemInterface) {
      return FALSE;
    }

    $normalizedFields = $snapshot->getNormalizedFields();

    $entityIsTranslatable = $entity->isTranslatable();
    $snapshotIsTranslatable = $snapshot->isTranslatable();
    $entityLanguages = $entity->getTranslationLanguages(FALSE);

    $referencesRestored = FALSE;

    // Some proprties that we may need to update may appear as computed or internal.
    /**
     * @var string $fieldName
     * @var FieldItemListInterface $fieldItemsList
     */
    foreach ($entity->getTypedData()->getProperties(TRUE) as $fieldName => $fieldItemsList) {
      if (empty($normalizedFields[$fieldName])) {
        continue;
      }

      $normalizedField = $normalizedFields[$fieldName];

      $referencesRestored = $this->handleField($fieldItemsList, $normalizedField) || $referencesRestored;

      if ($entityIsTranslatable && $snapshotIsTranslatable && $fieldItemsList->getFieldDefinition()->isTranslatable()) {
        foreach ($entityLanguages as $langcode => $language) {
          $normalizedTranslation = $snapshot->getTranslation($langcode);
          if (empty($normalizedTranslation[$fieldName])) {
            continue;
          }

          $normalizedFieldTranslation = $normalizedTranslation[$fieldName];

          $translation = $entity->getTranslation($langcode);
          if (!$translation) {
            continue;
          }

          $translationFieldItemsList = $translation->get($fieldName);

          $referencesRestored = $this->handleField($translationFieldItemsList, $normalizedFieldTranslation) || $referencesRestored;
        }
      }
    }

    return $referencesRestored;
  }

  /**
   * @param \Drupal\Core\Field\FieldItemListInterface $fieldItemsList
   * @param array $normalizedField
   *
   * @return bool
   *
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  private function handleField(FieldItemListInterface $fieldItemsList, array $normalizedField): bool {

    $fieldDefinition = $fieldItemsList->getFieldDefinition();
    if (!in_array($fieldDefinition->getType(), $this->getReferencingFields(), TRUE)) {
      return FALSE;
    }

    $fieldItemsList->setValue([]);
    $this->denormalizer->denormalize($normalizedField, get_class($fieldItemsList), YamlEncoder::FORMAT, [
      'target_instance' => $fieldItemsList,
      ContentSnapshotNormalizerContextTrait::$CONTENT_SNAPSHOT_CONTEXT => TRUE,
      ReferenceRestorerInterface::RESTORING_REFERENCES_CONTEXT => TRUE,
    ]);

    return TRUE;
  }

  /**
   * @return string[]
   */
  protected function getReferencingFields(): array {
    return [
      'entity_reference_revisions', // Requires looking up revisions id
      'layout_section', // Requires looking up revisions id
      ];
  }

}
