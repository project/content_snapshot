<?php

/**
 * @file
 */

namespace Drupal\content_snapshot\ReferenceRestorer;

use Drupal\content_snapshot\Snapshot\SnapshotItemInterface;

/**
 * Interface ReferenceRestorerInterface.
 */
interface ReferenceRestorerInterface {

  public const RESTORING_REFERENCES_CONTEXT = 'content_snapshot.restoring_references';

  /**
   * Restore object's references.
   *
   * @param \Drupal\content_snapshot\Snapshot\SnapshotItemInterface $snapshotItem
   * @param $object
   *
   * @return bool
   *  Returns object if restored references. Otherwise returns NULL.
   */
  public function restoreReferences(SnapshotItemInterface $snapshotItem, &$object): bool;

}
