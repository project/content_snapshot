<?php

/**
 * @file
 *
 * This field type allows us to "freeze" changed item, so it won't be updated
 * when we will be saving entities again, after resolving their relationships.
 *
 * This is required until https://www.drupal.org/project/drupal/issues/2329253
 * is resolved.
 */

namespace Drupal\content_snapshot\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\ChangedItem;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a 'changed' field type class that allows for disabling autoupdate.
 */
class FreezableChangedItem extends ChangedItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['content_snapshot_freeze'] = DataDefinition::create('boolean')
      ->setInternal(TRUE)
      ->setLabel(new TranslatableMarkup("Determines if the changed timestamp should be updated on the entity save."));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    if ($this->content_snapshot_freeze) {
      return;
    }

    parent::preSave();
  }

}
